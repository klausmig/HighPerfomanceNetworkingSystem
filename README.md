# High Perfomance Networking System

Write a simple calendar tool that allows the synchronization of appointments between different
hosts. It is sufficient to implement a text-based console application that provides facilities to add,
remove and modify appointments. Adding a graphical user-interface is optional.
The tool you are implementing needs to allow the interconnection of several machines to a
�calendar network� where all hosts share the same set of appointments. Changes on one machine
need to be propagated to all other machines. Note that you do not need to implement routing
facilities. For this purpose each host needs to know the addresses of all other hosts. A discovery
mechanism is also not necessary. New machines join the network by sending a register message
to one of the machines already in the network. The address of the new host is thereupon
propagated in the network. Hosts also need to be able to sign off from the network again


#TAGS: 

java, C#, WebServices, Concurrency, Eclipse, Visual Studio