﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

/// <summary>
/// Class to manage the collection of appointments
/// </summary>
[XmlRoot("AppointmentsCollection")]
public class Appointments
{

    /// <summary>
    /// List of appointments
    /// </summary>
    [XmlArray("Appointments")]
    [XmlArrayItem("Appointment")]
    public List<Appointment> assigntments = new List<Appointment>();
        
    /// <summary>
    /// Save the list of appointments into the local disk
    /// </summary>
    /// <param name="path"></param>
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(Appointments));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    /// <summary>
    /// Load from a file the set of appointments
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static Appointments Load(string path)
    {
        var serializer = new XmlSerializer(typeof(Appointments));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as Appointments;
        }
    }

    /// <summary>
    /// Loads the xml directly from the given string. Useful in combination with www.text.
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static Appointments LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(Appointments));
        return serializer.Deserialize(new StringReader(text)) as Appointments;
    }

}