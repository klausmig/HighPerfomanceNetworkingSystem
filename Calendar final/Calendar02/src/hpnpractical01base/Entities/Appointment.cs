﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Runtime.Serialization;

/// <summary>
/// Appointment Object
/// </summary>
[XmlRoot("Appointment")]
[DataContract] 
public class Appointment
{
    /// <summary>
    /// Unique id of the appointment 
    /// </summary>
    [DataMember]
    public int id;

    /// <summary>
    /// Date of the appointment
    /// </summary>
    [DataMember]
    public DateTime date;

    /// <summary>
    /// Creation Date of the appointment
    /// </summary>
    [DataMember]
    public DateTime creationDate;

    /// <summary>
    /// Duration
    /// </summary>
    [DataMember]
    public int duration;

    /// <summary>
    /// Header
    /// </summary>
    [DataMember]
    public String header;

    /// <summary>
    /// Comment
    /// </summary>
    [DataMember]
    public String comment;

    /// <summary>
    /// Convert the data from the Appointment into one string
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return "ID: " + id + ", Header: " + header + ", Comment: " + comment + ", Date: " + date.ToString() + ", Duration: " + duration;
    }
}