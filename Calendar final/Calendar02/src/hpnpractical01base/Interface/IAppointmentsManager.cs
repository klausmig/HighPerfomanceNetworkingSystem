using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.ServiceModel;


/// <summary>
/// Interface that define the methods that are publish by the server
/// </summary>
[ServiceContract]
public interface IAppointmentsManager
{
    /// <summary>
    /// This interface define the method to obtain the appointments from the server
    /// </summary>
    /// <returns></returns>
    [OperationContract(Action = "appointments.GetAppointments")]
    Appointment[] GetAppointments();

    /// <summary>
    /// This interface define the method to obtain the appointments from the server
    /// </summary>
    /// <returns></returns>
    [OperationContract(Action = "appointments.GetAppointmentsList")]
    string GetAppointmentsList();

    /// <summary>
    /// This method set the appointment collection of the server
    /// </summary>
    /// <param name="appointments"></param>
    /// <returns></returns>
    [OperationContract(Action = "appointments.SetAppointments")]
    bool SetAppointments(Appointment[] appointments);

    [OperationContract(Action = "appointments.SetAppointmentslist")]
    bool SetAppointmentslist(String appointmentsJson);

    [OperationContract(Action = "appointments.test")]
    bool test();

    /// <summary>
    /// Stablish the connection with the server
    /// </summary>
    /// <returns>true</returns>
    [OperationContract(Action = "appointments.GetConnection")]
    bool GetConnection();

    /// <summary>
    /// This method obtain an appointment by id 
    /// </summary>
    /// <param name="id"></param>
    /// <returns>Appointment</returns>
    [OperationContract(Action = "appointments.GetAppointmentById")]
    Appointment GetAppointmentById(int id);

    /// <summary>
    /// This method delete an Appointment from the server
    /// </summary>
    /// <param name="appointment"></param>
    [OperationContract(Action = "appointments.DeleteAppointment")]
    void DeleteAppointment(Appointment appointment);

    /// <summary>
    /// Method to register the new host and recibe the entire net 
    /// </summary>
    /// <returns></returns>
    [OperationContract(Action = "appointments.GetHosts")]
    string[] GetHosts();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [OperationContract(Action = "appointments.GetHostsString")]
    string GetHostsString();

    /// <summary>
    /// Method to register a new host
    /// </summary>
    [OperationContract(Action = "appointments.RegisterHost")]
    int RegisterHost(string host);

    /// <summary>
    /// Method to give the token >----------------------------------------------------------------------------------------------------------->
    /// </summary>
    [OperationContract(Action = "appointments.GiveToken")]
    bool GiveToken();

    /// <summary>
    /// Method to give the token >----------------------------------------------------------------------------------------------------------->
    /// </summary>
    [OperationContract(Action = "appointments.IsTokenNode")]//testing
    bool IsTokenNode();

    /// <summary>
    /// Set the collection of host in the server
    /// </summary>
    /// <param name="hosts"></param>
    [OperationContract(Action = "appointments.SetHosts")]
    bool SetHosts(string[] hosts);

    /// <summary>
    /// Set the collection of host in the server
    /// </summary>
    /// <param name="hosts"></param>
    [OperationContract(Action = "appointments.SetHostsString")]
    bool SetHostsString(string hosts);

    /// <summary>
    /// Lock the client so the user cant do any changes on appointments
    /// </summary>
    /// <param name="hosts"></param>
    //[OperationContract(Action = "appointments.LockClient")]
    //bool LockClient(bool locked);



    // Accept a request resource from a client

    [OperationContract(Action = "appointments.requestResource")]
    String requestResource(String message);


    // Accept a ok reply release from a client

    [OperationContract(Action = "appointments.replyOK")]
    int replyOK(String messageClient);



    





}
