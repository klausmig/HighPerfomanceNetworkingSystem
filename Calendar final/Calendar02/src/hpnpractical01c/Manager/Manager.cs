﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

/// <summary>
/// Manage the working of the system
/// </summary>
public static class Manager
{
    /// <summary>
    /// Path where the file is store localy
    /// </summary>
    public static string sName = ConfigurationSettings.AppSettings["FilePath"];
    /// <summary>
    /// Class to manage the network 
    /// </summary>
    public static Network network = new Network();

    /// <summary>
    /// This method consult all the appointments in the system.
    /// </summary>
    /// <returns></returns>
    public static Appointments getAppointments()
    {
        if (!File.Exists(sName))
            return new Appointments();

        return Appointments.Load(sName);
    }

    /// <summary>
    /// This Methos get the las id in the system.
    /// </summary>
    /// <returns></returns>
    public static int getLastId()
    {
        Appointments aux = Manager.getAppointments();
        int highId = 0;

        //There is pending the implementation with the sincronization between servers.
		foreach (Appointment a in aux.assigntments) {
			if (a.id > highId) {
				highId = a.id;
			}
		}
		highId++;

		return highId;
    }

    /// <summary>
    /// This method save an appointment into the system.
    /// </summary>
    /// <param name="newAppointment"></param>
    public static void saveAppointment(Appointment newAppointment)
    {
        Appointments aux = Manager.getAppointments();
        aux.assigntments.Add(newAppointment);
        //There is pending the sync between the servers.
        aux.Save(sName);

        //network.lockHosts(true);
        network.SynchronizeAppointment(aux);
        //network.lockHosts(false);
    }

    /// <summary>
    /// This method obtain an Appointment from the system.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public static Appointment getAppointmentById(int id)
    {
        Appointments aux = Manager.getAppointments();
        foreach (Appointment a in aux.assigntments)
        {
            if (a.id == id)
            {
                return a;
            }
        }
        return null;
    }

    /// <summary>
    /// This method propagates the update of an appointment
    /// </summary>
    /// <param name="appointment"></param>
    public static void updateAppointment(Appointment appointment)
    {
        Appointments aux = Manager.getAppointments();
        foreach (Appointment a in aux.assigntments)
        {
            if (a.id == appointment.id)
            {
                a.comment = appointment.comment;
                a.date = appointment.date;
                a.duration = appointment.duration;
                a.header = appointment.header;
            }
        }
        aux.Save(sName);
        //There is still missing the propagation throug the network

       // network.lockHosts(true);
        network.SynchronizeAppointment(aux);
       // network.lockHosts(false);

    }

    /// <summary>
    /// Delete an Appointment from the system by id
    /// </summary>
    /// <param name="id"></param>
    public static void deleteAppointment(int id)
    {
        Appointments aux = Manager.getAppointments();
        Appointment auxDel = new Appointment();
        foreach (Appointment a in aux.assigntments)
        {
            if (a.id == id)
            {
                auxDel = a;
            }
        }
        aux.assigntments.Remove(auxDel);
        aux.Save(sName);

       // network.lockHosts(true);
        network.SynchronizeAppointment(aux);
       // network.lockHosts(false);
    }

    /// <summary>
    /// Delete an Appointment from the system
    /// </summary>
    /// <param name="appointment"></param>
    public static void deleteAppointment(Appointment appointment)
    {
        Appointments aux = Manager.getAppointments();
        aux.assigntments.Remove(appointment);
        aux.Save(sName);

       // network.lockHosts(true);
        network.SynchronizeAppointment(aux);
       // network.lockHosts(false);

    }

    /// <summary>
    /// This method set the set of appointments
    /// </summary>
    /// <param name="appointments"></param>
    /// <returns></returns>
    public static Boolean setApointments(Appointments appointments)
    {
        try 
        {
            if (appointments != null)
            {
                appointments.Save(sName);
            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
