﻿using System;
using HPNPractical01C;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Configuration;
using Newtonsoft.Json;
using System.Threading;

public static class TokenManager
{

    public static bool alreadyRequestToken = false;
    public static bool haveTheToken = false;
    public static bool usingToken = false;
    public static String nextHost = null;

    public static bool onlyNodeInRing = true;
    public static bool isServerNode = false;//testing
    private static int tokenCheckerCounter = 0;//testing

    public static Thread thread = new Thread(DoWorkThread);

    public static void DoWorkThread()
    {
        while (thread.IsAlive)
        {
            Thread.Sleep(200);

            if (haveTheToken && !usingToken && Manager.network.hosts.Count > 1)
            {
                passToken();
            }
            else if (Manager.network.hosts.Count > 1)
            {
                onlyNodeInRing = false;
            }
            else if (!onlyNodeInRing)
            {
                Console.WriteLine("\n>>This is the only host in the Network\n");
                onlyNodeInRing = true;
            }
            //if (isServerNode && !haveTheToken)
            //{
            //    checkTokenNode();
            //}
        }
    }

    public static void checkTokenNode()
    {
        if(tokenCheckerCounter == 2)
        {
            tokenCheckerCounter = 0;
            tokenArrival();
        }
        else if (Manager.network.hosts.Count > 1)
        {
            bool tokenNode = false;
            String localHost = Manager.network.getLocalHost();

            foreach (string host in Manager.network.hosts)
            {
                if (localHost != host)
                {
                    Uri serverAddress = new UriBuilder(host).Uri;

                    ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                    serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                    IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                    try
                    {
                        bool ret = appointmentsAPI.GetConnection();
                        if (ret)
                        {
                            if (appointmentsAPI.IsTokenNode()) tokenNode = true;
                        }
                        serverAPIFactory.Close();
                    }
                    catch 
                    {
                        //Do nothing...
                    }
                }
            }

            if (!tokenNode)
            {
                tokenCheckerCounter++;
            }
        }
    }

    public static void passToken()
    {

        usingToken = false;
        alreadyRequestToken = false;

        calculateNextHost();
        Uri serverAddress = new UriBuilder(nextHost).Uri;
        ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
        serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());
        IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

        try
        {
            bool ret = appointmentsAPI.GetConnection();
            if (ret)
            {
                //resetting values
                haveTheToken = false;
                appointmentsAPI.GiveToken();
            }
            serverAPIFactory.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("\n>>Error Connecting to host:" + nextHost + " ... Deleting ...\n");
            Manager.network.removeHost(nextHost);
            Manager.network.SynchronizeHost();
        }
    }


    public static void calculateNextHost()
    {
        String localHost = Manager.network.getLocalHost();
        String[] strArray = Manager.network.hosts.ToArray();
        if (Manager.network.hosts.Count > 1)
        {
            for (int i = 0; i < strArray.Length; i++)
            {
                if (strArray[i] == localHost && i != (strArray.Length - 1))
                {
                    nextHost = strArray[i + 1];
                }
                else if (strArray[i] == localHost && i == (strArray.Length - 1))
                {
                    nextHost = strArray[0];
                }
            }
        }
        else
        {
            nextHost = null;
        }
        
    }

    public static void tokenArrival()
    {
        if (alreadyRequestToken && !haveTheToken)
        {
            haveTheToken = true;
            usingToken = true;
            Console.WriteLine("\n> You have the token, you can access the resource... \n");
            Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
        }
        else
        {
            haveTheToken = true;
        }
    }

    public static void startThread()
    {
        thread.IsBackground = true;
        thread.Start();
    }


}
