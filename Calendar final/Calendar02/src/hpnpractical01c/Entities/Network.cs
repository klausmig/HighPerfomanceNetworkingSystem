﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Configuration;
using Newtonsoft.Json;

/// <summary>
/// Network class
/// </summary>
public class Network
{
    /// <summary>
    /// Host that are part of the network
    /// </summary>
    public List<string> hosts = new List<string>();



    public string ipLocal = "";

    public Ricart algRicart = new Ricart(); // CARE!


    /// <summary>
    /// State of the net, true: connect, false: disconnect
    /// </summary>
    public Boolean state=false;

   

    /// <summary>
    /// This methos add a new host
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    public Boolean addHost(string url)
    {
        hosts.Add(url);
        return true;
    }

    /// <summary>
    /// This method removes a host from the collection
    /// </summary>
    /// <param name="host"></param>
    /// <returns></returns>
    public Boolean removeHost(string host)
    {
        hosts.Remove(host);
        return true;
    }

    // This method test the connection to a host using LC 

    public Boolean connectHost(string host) // EDITED FOR R&A**********
    {
        Uri serverAddress = new UriBuilder(host).Uri;

        Manager.network.algRicart.LC = 0; // reset Local Clock

        ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
        serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

        IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();
        
        try
        {
            bool ret = appointmentsAPI.GetConnection();
            if (ret) {
                Console.WriteLine("\n>>to server: {0} success! ", host);

                hosts.Add(host);

                //Register the local host into the network
                string local = getLocalHost();

                int LCServer = appointmentsAPI.RegisterHost(local); //Execute RegisterHost on server : Add to a network its own

                algRicart.LC = Math.Max(algRicart.LC, LCServer) + 1;
                Console.WriteLine(">>LC: {0} , LCServer:{1}\n ",algRicart.LC, LCServer );

                //hosts = appointmentsAPI.GetHosts().ToList(); //Get all the host to be syncronized

                string hostsString = appointmentsAPI.GetHostsString();
                List<string> hostList = JsonConvert.DeserializeObject<List<string>>(hostsString);

                hosts = hostList; //Get all the host to be syncronized
            }

            serverAPIFactory.Close();
            SynchronizeHost();
            state = true;
            return ret;
        }        
        catch (Exception e)
        {
            Console.WriteLine(">>Error Connecting :" + host+"\n");
            return false;
        }
    }


    /// This methos syncrhonize all the hosts
    public void SynchronizeHost()
    {
        String localHost =getLocalHost();

        Console.WriteLine("\n>>Synchronizing other Address Host ...\n ");
        foreach (string host in hosts)
        {
            if (localHost != host)
            {
                Uri serverAddress = new UriBuilder(host).Uri;

                ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                try
                {
                    bool ret = appointmentsAPI.GetConnection();
                    if (ret)
                    {
                        Console.WriteLine(">>The Synchronizing with {0} sucess! ", host);

                        //appointmentsAPI.SetHosts(hosts.ToArray<string>());
                        appointmentsAPI.SetHostsString(JsonConvert.SerializeObject(hosts.ToArray<string>()));

                    }
                    serverAPIFactory.Close();
                }
                catch (Exception e)
                {
                    // Console.WriteLine(">>Error Connecting: {0}\n", e.ToString());
                    Console.WriteLine(">>Error Connecting to host :"+host+"\n");
                }
            }
        }
    }

    /// <summary>
    /// This methos syncrhonize a deleted appointment in all the hosts
    /// </summary>
    public void SynchronizeAppointment(Appointments newAppointments)
    {

        Console.WriteLine("\n>>Synchronizing Appointments in other Host ...\n ");
        String localHost = getLocalHost();

        List<string> hostsNew = new List<string>();


        foreach (string host in hosts)
        {
            if (localHost != host)
            {
                Uri serverAddress = new UriBuilder(host).Uri;

                ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                try
                {
                    bool ret = appointmentsAPI.GetConnection();
                    if (ret)
                    {
                        Console.WriteLine(">>Synchronizing Appointments in: {0} ,was successful ", host);

                        //appointmentsAPI.SetAppointments((newAppointments.assigntments).ToArray<Appointment>());
                        appointmentsAPI.SetAppointmentslist(JsonConvert.SerializeObject(newAppointments.assigntments));
                        hostsNew.Add(host);
                    }
                    serverAPIFactory.Close();
                }
                catch (Exception e)
                {
                   // Console.WriteLine("Error Connecting: {0}", e.ToString());
                    Console.WriteLine(">>Error Connecting to host:" + host+" ... Deleting ...\n");
                    hosts.Remove(host);
                }
            }
        }
        hostsNew.Add(localHost);
        hosts = hostsNew;
        SynchronizeHost();
    }


    /// <summary>
    /// This method get the collection of appointments from the server
    /// </summary>
    /// <param name="host"></param>
    /// <returns></returns>
    public Appointments getAppointments(string host)
    {
        Uri serverAddress = new UriBuilder(host).Uri;

        ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
        serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

        IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();
        
        try
        {
            //aux = svr.GetAppointments();
           // Appointment[] appointmentsResponse = appointmentsAPI.GetAppointments();
            //Appointments appointmentsAux = new Appointments();
            //appointmentsAux.assigntments = appointmentsResponse.ToList<Appointment>();

            List<Appointment> appointmentsResponse = JsonConvert.DeserializeObject<List<Appointment>>(appointmentsAPI.GetAppointmentsList());
            Appointments appointmentsAux = new Appointments();
            appointmentsAux.assigntments = appointmentsResponse;


            Console.WriteLine(">>The number of appointmens in the server is: {0}\n", appointmentsAux.assigntments.Count);
            serverAPIFactory.Close();
            
            return appointmentsAux;
        }
        catch (Exception e)
        {
            Console.WriteLine("Error Connecting to host: "+host+"\n");
            return null;
        }
    }


    public string getLocalHost(){
        /*string direction;
        WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
        WebResponse response = request.GetResponse();
        StreamReader stream = new StreamReader(response.GetResponseStream());
        direction = stream.ReadToEnd();
        stream.Close();
        response.Close(); //Search for the ip in the html
        int first = direction.IndexOf("Address: ") + 9;
        int last = direction.LastIndexOf("");
        direction = direction.Substring(first, last - first);*/
        
        
       // Console.WriteLine(Dns.GetHostName());
        IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
        string ip = "";
        foreach (IPAddress addr in localIPs)
        {
            if (addr.AddressFamily == AddressFamily.InterNetwork)
            {
                ip = addr.ToString();
              //  Console.WriteLine(addr);
            }
        }

        //ipLocal = ip;

       // Console.WriteLine("iplocal:"+ip);

        ipLocal= ConfigurationSettings.AppSettings["LocalIp"];
        return "http://" + ip + ":" + ConfigurationSettings.AppSettings["portNumber"] + "/" + ConfigurationSettings.AppSettings["portName"] + "/" + ConfigurationSettings.AppSettings["portEndPoint"];
    }

    public string getLocalIp()
    {


       // Console.WriteLine(Dns.GetHostName());
        IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
        string ip = "";
        foreach (IPAddress addr in localIPs)
        {
            if (addr.AddressFamily == AddressFamily.InterNetwork)
            {
                ip = addr.ToString();
               // Console.WriteLine(addr);
            }
        }

        ipLocal = ip;
        return ip;
    }


}
