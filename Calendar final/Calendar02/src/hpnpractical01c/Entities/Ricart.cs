﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Collections;
using System.ServiceModel;
using Microsoft.Samples.XmlRpc;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Configuration;
using Newtonsoft.Json;


public class Ricart
{
    //Logic Clock
    public int LC=0;
    public List<string> requestQueue,requestIPServer;
    public int LCRequest = 0;

    // list of ipadress denoates the nodes with reply not ok, only with all nodes replying ok the actual node will accesss the resource.
    public List<string> replyIPNotOk;
    //Dictionary<string, bool> replyIPNotOk;

    public bool alreadyRequestResource = false,usingResource=false;


   
    // Request resource to all online nodes in hosts list.

    public void requestResource(List<string> hosts)
    {
        replyIPNotOk = new List<string>();
        requestQueue = new List<string>();

        Console.WriteLine("\n>>Requesting Editing Appointment Resource to all nodes  ... ");
        
        String localHost = Manager.network.getLocalHost();
        List<string> hostsNew = new List<string>();

        foreach (string host in hosts)
        {

            if (localHost != host)
            {
                Uri serverAddress = new UriBuilder(host).Uri;

                ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                try
                {
                    bool ret = appointmentsAPI.GetConnection();
                    if (ret)
                    {
  
                        LC++;
                        LCRequest = LC;
                        String msg= appointmentsAPI.requestResource(LC + "," + localHost);
                       // Console.WriteLine("requestResource msg reply:" + msg + " from " + host);

                        string[] message = msg.Split(',');

                       // Console.WriteLine(message.ToString());
                        LC = Math.Max( Convert.ToInt32(message[0]),LC) + 1;

        

                       // if (message[1] != "ok")
                        if (!(message[1].Equals("ok", StringComparison.OrdinalIgnoreCase)))
                        {
                            /*
                            Console.WriteLine("\n  Host {0} replied NotOK replyIPNotOk: ", host);
                            foreach (string url in replyIPNotOk)
                            {


                                Console.WriteLine("\n" + url);

                            }*/
                            if (!(replyIPNotOk.Contains(host)))
                                replyIPNotOk.Add(host); //Some Node has less LC than this node
                        }

                        else
                        {
                            //Console.WriteLine("\n  Host {0} replied OK requestQueue: ", host);
                            foreach (string url in requestQueue)
                            {


                                Console.WriteLine( url);

                            }

                            if (!(requestQueue.Contains(host)))
                            {
                                requestQueue.Add(host); //Add nodes replied ok so they have more lC and are in the list
                                Console.WriteLine("\n  Host {0} replied OK ", host);
                            }

                            hostsNew.Add(host);

                        }
                    }
                    serverAPIFactory.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine("\n>>Error Connecting to host:" + host + " ... Deleting ...\n");
                }
            }
        }

       // Console.WriteLine("\n");
        hostsNew.Add(localHost);
        hosts = hostsNew;

        alreadyRequestResource = true;

        
        //Manager.network.SynchronizeHost();
        if (replyIPNotOk.Count() > 0)
        {
            Console.WriteLine("\n>Waiting ok reply from hosts:\n ");
            foreach (string host in replyIPNotOk)
            {
                Console.WriteLine("\n>> {0} \n ", host);

            }
        }
        else {

           usingResource = true;
           alreadyRequestResource = false;
        }

      /*  if (replyIPNotOk.Count() == 0)
        {
            usingResource = true;
            alreadyRequestResource = false;
        }*/

    }

    //When finish using the resource reply Ok to all nodes in the list 
    public void replyOK()
    {


        Console.WriteLine("\n>>Replying Ok release to all nodes in the list nodes  ... \n");
        String localHost = Manager.network.getLocalHost();
        List<string> hostsNew = new List<string>();

        foreach (string host in requestQueue)
        {

            if (localHost != host)
            {
                Uri serverAddress = new UriBuilder(host).Uri;

                ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                try
                {
                    bool ret = appointmentsAPI.GetConnection();
                    if (ret)
                    {

                        LC++;

                        int LCClient = appointmentsAPI.replyOK(LC + "," + localHost);



                        LC = Math.Max(LCClient, LC) + 1;


                        
                        Console.WriteLine("\n  to Host {0} sended OK ", host);
                        


                        hostsNew.Add(host);

                        
                    }
                    serverAPIFactory.Close();
                }
                catch (Exception e)
                {
                    Console.WriteLine(">>Error Connecting to host:" + host + " ... Deleting ...\n");
                }
            }
        }

        Console.WriteLine("\n");
        Console.Write("LC:" + LC + ">>");
        alreadyRequestResource = false;
        usingResource = false;


    }


    // This method cleans the servers those we are waiting for a ok response, sometimes some servers are down
    // Recover algorithm Ricart
   
    public void CleanReplyNotOkList()
    {

        Console.WriteLine("\n>>Updating response Ok from Host list notOk ...\n ");
        String localHost = Manager.network.getLocalHost();

        List<string> hostsNew = new List<string>();


        foreach (string host in replyIPNotOk)
        {
            if (localHost != host)
            {
                Uri serverAddress = new UriBuilder(host).Uri;

                ChannelFactory<IAppointmentsManager> serverAPIFactory = new ChannelFactory<IAppointmentsManager>(new WebHttpBinding(WebHttpSecurityMode.None), new EndpointAddress(serverAddress));
                serverAPIFactory.Endpoint.Behaviors.Add(new XmlRpcEndpointBehavior());

                IAppointmentsManager appointmentsAPI = serverAPIFactory.CreateChannel();

                try
                {
                    bool ret = appointmentsAPI.GetConnection();
                    if (ret)
                    {
                        hostsNew.Add(host);
                    }
                    serverAPIFactory.Close();
                }
                catch (Exception e)
                {
                    // Console.WriteLine("Error Connecting: {0}", e.ToString());
                    Console.WriteLine(">>Error Connecting to host:" + host + " ... Deleting ...\n");
                    // hosts.Remove(host);
                }
            }
        }
        //hostsNew.Add(localHost);
        replyIPNotOk = hostsNew;
        
    }


}

