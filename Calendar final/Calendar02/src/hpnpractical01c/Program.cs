﻿using HPNPractical01C.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace HPNPractical01C
{
    class Program
    {
        public static AppointmentsServer server = new AppointmentsServer();
       
        public static bool algTypeTR = false;
       
        static void Main(string[] args)
        {
            //Initialize the server to be able to work as a server
            server = new AppointmentsServer();
            server.RunServer();

            //Check algorithm type and set it as true if is Token Ring ----------------------------------->
            if (server.algType.Equals("tr"))
            {
                algTypeTR = true;
            }

            //Declaring and inicialize handlers for app closing events ----------------------------------->
            HandlerRoutine hr = new HandlerRoutine(ConsoleCtrlCheck);
            GC.KeepAlive(hr);
            SetConsoleCtrlHandler(hr, true);

            //Starting token for 1st time and starting thread          ----------------------------------->
            if (algTypeTR)
            {
                if (Manager.network.hosts.Count <= 1 )
                {
                    TokenManager.haveTheToken = true;
                    TokenManager.isServerNode = true;//testing
                    TokenManager.startThread();
                }
                else 
                {
                    TokenManager.startThread();
                }
            }

            int menuOptHom = 0; //Option Menu Home
            int menuOptApp = 0; //Option Menu Appointments
            int menuOptNet = 0; //Option Menu Network

            while (menuOptHom != 4){//Home Menu

                menuOptHom = 0; //Option Menu Home
                menuOptApp = 0; //Option Menu Appointments
                menuOptNet = 0; //Option Menu Network

                menuOptHom = InitMenu();

                if (menuOptHom == 1)  {//Option to Manage the Appointments

                    while (menuOptApp != 5) {

                        menuOptApp = AppointmentsMenu();

                        if ((menuOptApp == 2) || (menuOptApp == 3) || (menuOptApp == 1))
                        {
                            if (!algTypeTR) // Starting with R&A Algorithm
                            {
                                if (!Manager.network.algRicart.alreadyRequestResource && !Manager.network.algRicart.usingResource)
                                    Manager.network.algRicart.requestResource(Manager.network.hosts);
                                else if (Manager.network.algRicart.alreadyRequestResource && !Manager.network.algRicart.usingResource)
                                {
                                    Manager.network.algRicart.CleanReplyNotOkList(); // Maybe some servers are down recover algorithm

                                    if (Manager.network.algRicart.replyIPNotOk.Count() > 0)
                                    {
                                        Console.WriteLine("\n>Accessing resource ... Waiting ok reply from hosts: ");
                                        foreach (string host in Manager.network.algRicart.replyIPNotOk)
                                        {
                                            Console.WriteLine("\n>> {0} ", host);

                                        }
                                        Console.WriteLine("\n");
                                    }
                                    else
                                    {

                                        Console.WriteLine("\n>> You have  the edit or delete resource, you can proceed !");
                                        Manager.network.algRicart.alreadyRequestResource = false;
                                        Manager.network.algRicart.usingResource = true;

                                    }

                                }
                            }
                            else // Starting with Token Ring Algorithm      ----------------------------------->
                            {
                                if (!TokenManager.alreadyRequestToken && !TokenManager.haveTheToken)
                                {
                                    TokenManager.alreadyRequestToken = true;
                                    Console.WriteLine("\n> Waiting for token... ");
                                }
                                else if (TokenManager.alreadyRequestToken && !TokenManager.haveTheToken)
                                {
                                    Console.WriteLine("\n> Still waiting for token... ");
                                }
                                else
                                {
                                    TokenManager.usingToken = true;
                                    TokenManager.alreadyRequestToken = false;
                                }
                            }
                        
                        }
                        if ((menuOptApp == 1) && Manager.network.algRicart.usingResource && !algTypeTR) 
                        {
                            createAppointmentMenu();
                            Manager.network.algRicart.replyOK(); //Release the resource
                        }
                        else if ((menuOptApp == 1) && TokenManager.haveTheToken && algTypeTR) // TOKEN RING CODE ---------------------------->
                        {
                            createAppointmentMenu();
                            TokenManager.passToken(); //passig the token
                        }
                        else if ((menuOptApp == 2) && Manager.network.algRicart.usingResource && !algTypeTR)
                        {
                            deleteAppointment();
                            Manager.network.algRicart.replyOK(); //Release the resource
                        }
                        else if ((menuOptApp == 2) && TokenManager.haveTheToken && algTypeTR) // TOKEN RING CODE ---------------------------->
                        {
                            deleteAppointment();
                            TokenManager.passToken(); //passig the token
                        }
                        else if ((menuOptApp == 3) && Manager.network.algRicart.usingResource && !algTypeTR)
                        {
                            modifyAppointment();
                            Manager.network.algRicart.replyOK(); //Release the resource
                        }
                        else if ((menuOptApp == 3) && TokenManager.haveTheToken && algTypeTR) // TOKEN RING CODE ---------------------------->
                        {
                            modifyAppointment();
                            TokenManager.passToken(); //passig the token
                        }
                        else if (menuOptApp == 4) {
                            listAppointmentsMenu();
                        }
                        else if (menuOptApp == 6)
                        {
                            menuOptApp = 5;
                            menuOptHom = 4;
                        }
                    }
                }
                else if (menuOptHom == 2)
                {
                    while (menuOptNet != 4)
                    {
                        menuOptNet = NetworkMenu();
                        if (menuOptNet == 1) { 
                            if(connectMenu()){
                                Manager.network.state = true;
                            }
                            else{
                                Manager.network.state = false;
                            }
                        }
                        else if (menuOptNet == 2) {

                            if (TokenManager.haveTheToken == true && Manager.network.hosts.Count > 1) // TOKEN RING CODE ---------------------------->
                            {
                                TokenManager.passToken();
                            }
                            Manager.network.state = false;
                            Manager.network.removeHost(Manager.network.getLocalHost());
                            Manager.network.SynchronizeHost();
                            Manager.network.hosts = new List<string>();

                        
                        
                        }
                        else if (menuOptNet == 3) { 

                            foreach (string host in  Manager.network.hosts)
                            {
                                Console.Write(host+"\n");
                        
                            }
                            //String linea = Console.ReadLine();
                        
                        
                        }
                        else if (menuOptNet == 5) 
                        {
                            menuOptNet = 4;
                            menuOptHom = 4;
                        }
                    }
                }
                else if (menuOptHom == 3)//Exit
                {
                    AboutMenu();
                }
            }
            if (TokenManager.haveTheToken == true && Manager.network.hosts.Count > 1) // TOKEN RING CODE ---------------------------->
            {
                TokenManager.passToken();
            }
            if (algTypeTR)
            {
                //TokenManager.thread.Abort();
                //TokenManager.updateRing(Manager.network.getLocalHost());
                Manager.network.hosts.Remove(Manager.network.getLocalHost());
                Manager.network.SynchronizeHost();

            }
            server.closeServer();
        }

        //Closing event handler <<<<<<<<<<<<<<<<<<
        [DllImport("Kernel32")]
        public static extern bool SetConsoleCtrlHandler(HandlerRoutine Handler, bool Add);
        public delegate bool HandlerRoutine(CtrlTypes CtrlType);
        public enum CtrlTypes
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT,
            CTRL_CLOSE_EVENT,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT
        }
        private static bool ConsoleCtrlCheck(CtrlTypes ctrlType)
        {
            if (TokenManager.haveTheToken == true && Manager.network.hosts.Count > 1) // TOKEN RING CODE ---------------------------->
            {
                TokenManager.passToken();
            }
            if (algTypeTR)
            {
                //TokenManager.updateRing(Manager.network.getLocalHost());
                Manager.network.hosts.Remove(Manager.network.getLocalHost());
                Manager.network.SynchronizeHost();
            }
            server.closeServer();
            return true;
        }

        /**
         * Init Menu 
         */
        public static int InitMenu()
        {
           // Console.Clear();
            Console.Write("\n****************************************************************\n");
            Console.Write("****************************************************************\n");
            Console.Write("**************    HPN: Practical Assigntment 2    **************\n");
            Console.Write("**************         C# Solution v0.5.3         **************\n");
            if (algTypeTR)
            {
                Console.Write("**************      Token Ring Implementation     **************\n");
            }
            else
                Console.Write("**************         Ricart & Agrawala          **************\n");

            Console.Write("****************************************************************\n");
            Console.Write("****************************************************************\n");
            Console.Write("**************     OPTIONS:                       **************\n");
            Console.Write("**************         1. Manage Appointments     **************\n");
            Console.Write("**************         2. Manage Network          **************\n");
            Console.Write("**************         3. About us                **************\n");
            Console.Write("**************         4. Exit                    **************\n");
            Console.Write("****************************************************************\n");
            Console.Write("****************************************************************\n\n");
            Console.Write("URL Server: " + server.ServerAddress+"\n");
            Console.Write("LOCAL IP: " + server.localIpAddress + "\n\n");

            if (!algTypeTR)
            {
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
            }
            else
            {
                Console.Write("Hosts:"+Manager.network.hosts.Count+">>");
            }

            String linea = Console.ReadLine();

            int inputConsole = 0;
            try
            {
                inputConsole = int.Parse(linea);
            }
            catch {
                Console.Write(">>Invalid Option\n\n>>");
            }
            return inputConsole;
        }

        /**
         * About Menu 
         */
        public static void AboutMenu()
        {
            // Console.Clear();
            Console.Write("****************************************************************\n");
            Console.Write("**************     GROUP MEMBERS:                 **************\n");
            Console.Write("**************         Cristobal Leiva            **************\n");
            Console.Write("**************         Klaus Martinez             **************\n");
            Console.Write("**************         Carlos Montoya             **************\n");
            Console.Write("**************         Ahmad Amayri               **************\n");
            Console.Write("****************************************************************\n");
            Console.Write("**************   To continue press enter          **************\n");
            Console.Write("****************************************************************\n\n");
            if (!algTypeTR)
            {
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
            }
            else
            {
                Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
            }
            String linea = Console.ReadLine();
        }

        /**
         * Appointment Menu 
         */
        public static int AppointmentsMenu()
        {
           // Console.Clear();
            Console.Write("\n****************************************************************\n");
            Console.Write("**************   Appointments Menu:               **************\n");
            Console.Write("****************************************************************\n");
            Console.Write("**************     OPTIONS:                       **************\n");
            if (Manager.network.state && Manager.network.hosts.Count>1)
            {
                Console.Write("**************         1. Create Appointment      **************\n");
                Console.Write("**************         2. Delete Appointment      **************\n");
                Console.Write("**************         3. Modify Appointment      **************\n");
            }
            Console.Write("**************         4. List Appointments       **************\n");
            Console.Write("**************         5. Return to Home          **************\n");
            Console.Write("**************         6. Exit                    **************\n");
            Console.Write("****************************************************************\n\n");
            if (!algTypeTR)
            {
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
            }
            else
            {
                Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
            }

            String linea = Console.ReadLine();
            int inputConsole = 0;
            try
            {
                inputConsole = int.Parse(linea);
            }
            catch
            {
                Console.Write(">>Invalid Option\n\n>>");
            }
            return inputConsole;
        }

        /**
          * Networking Menu 
          */
        public static int NetworkMenu()
        {
           // Console.Clear();
            Console.Write("****************************************************************\n");
            Console.Write("**************   Network Menu:                    **************\n");
            Console.Write("****************************************************************\n");
            Console.Write("**************     OPTIONS:                       **************\n");
            Console.Write("**************         1. Connect                 **************\n");
            Console.Write("**************         2. Disconnect               **************\n");
            Console.Write("**************         3. List Host               **************\n");
            Console.Write("**************         4. Return to Home          **************\n");
            Console.Write("**************         5. Exit                    **************\n");
            Console.Write("****************************************************************\n\n");
            if (!algTypeTR)
            {
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
            }
            else
            {
                Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
            }

            String linea = Console.ReadLine();
            int inputConsole = 0;
            try
            {
                inputConsole = int.Parse(linea);
            }
            catch
            {
                Console.Write(">>Invalid Option\n\n>>");
            }
            return inputConsole;
        }


        /**
         * This method show all the appointments
         */ 
        public static void listAppointmentsMenu()
        {
            Appointments list = new Appointments();
            list = Manager.getAppointments();

            Console.Clear();
            Console.Write("****************************************************************\n");
            Console.Write("**************         Appointments List:         **************\n");
            Console.Write("****************************************************************\n\n");
            if (list.assigntments != null && list.assigntments.Count > 0) {  
                foreach(Appointment a in list.assigntments)
                {
                    Console.Write(a.ToString() + "\n");
                }
            }
            else
            {
                Console.Write("**************  No Appointments in the system     **************\n");                
            }

            Console.Write("\n****************************************************************\n");
            Console.Write("**************   To continue press enter          **************\n");
            Console.Write("****************************************************************\n\n");
            String linea = Console.ReadLine();

            //Console.SetWindowSize(origWidth, origHeight);
        }

        /**
         * This method creates an Appointment 
         */ 
        public static void createAppointmentMenu()
        {
            Console.Clear();
            Console.Write("****************************************************************\n");
            Console.Write("****   Please enter the Header of the Appointment:    **********\n");
            Console.Write("****************************************************************\n");
            String header = Console.ReadLine();

            Console.Write("\n****************************************************************\n");
            Console.Write("****   Please enter the Comments of the Appointment:   *********\n");
            Console.Write("****************************************************************\n");
            String comment = Console.ReadLine();

            Boolean opc = false;
            String date = "";
            while (!opc) { 
                Console.Write("\n****************************************************************\n");
                Console.Write("****   Please enter the Date of the Appointment        *********\n");
                Console.Write("****   The Date should have the format YYYY.MM.DD HH:MM   ******\n");
                Console.Write("****************************************************************\n");
                date = Console.ReadLine();
                opc = UtilDate.validateTimeFromString(date);
            }

            DateTime dateTime = UtilDate.getTimeFromString(date);

            Console.Write("\n****************************************************************\n");
            Console.Write("****   Please enter the Duration of the Appointment:   *********\n");
            Console.Write("****************************************************************\n");
            int duration = 0;
            while (true)
            {
                String line = Console.ReadLine();
                if (Validator.isNumber(line))
                {
                    duration = int.Parse(line);
                    break;
                }
                Console.Write(">>Invalid Option: Insert a Number \n>>");
            }

            int id = Manager.getLastId();

            Appointment newAppointment = new Appointment();
            newAppointment.id = id;
            newAppointment.comment = comment;
            newAppointment.date = dateTime;
            newAppointment.creationDate = DateTime.Now;
            newAppointment.duration = duration;
            newAppointment.header = header;

            Manager.saveAppointment(newAppointment);
        }

        /*
         * This Method show the info of an appointment
         */ 
        public static void showAppointment(Appointment appointment)
        {
            Console.Write("\n****************************************************************\n");
            Console.Write("      Id: " + appointment.id + "\n");
            Console.Write("  Header: " + appointment.header + "\n");
            Console.Write(" Comment: " + appointment.comment + "\n");
            Console.Write("    Date: " + appointment.date.ToString() + "\n");
            Console.Write("Duration: " + appointment.duration + "\n");
            Console.Write("****************************************************************\n\n");
        }

        /**
         * This method modify and existing appointment
         */ 
        public static void modifyAppointment()
        {
            Console.Clear();
            Console.Write("****************************************************************\n");
            Console.Write("****  Please enter the ID of the Appointment to consult:   *****\n");
            Console.Write("****************************************************************\n");
            int id = 0;
            while (true)
            {
                String line = Console.ReadLine();
                if (Validator.isNumber(line))
                {
                    id = int.Parse(line);
                    break;
                }
                Console.Write(">>Invalid Option: Insert a Number \n>>");
            }
            Appointment aux = Manager.getAppointmentById(id);
            showAppointment(aux);

            Console.Write("\n****************************************************************\n");
            Console.Write("****   Please enter the Header of the Appointment:    **********\n");
            Console.Write("****************************************************************\n");
            String header = Console.ReadLine();

            Console.Write("\n****************************************************************\n");
            Console.Write("****   Please enter the Comments of the Appointment:   *********\n");
            Console.Write("****************************************************************\n");
            String comment = Console.ReadLine();

            Boolean opc = false;
            String date = "";
            while (!opc)
            {
                Console.Write("\n****************************************************************\n");
                Console.Write("****   Please enter the Date of the Appointment        *********\n");
                Console.Write("****   The Date should have the format YYYY.MM.DD HH:MM   ******\n");
                Console.Write("****************************************************************\n");
                date = Console.ReadLine();
                if (date.Length == 0) opc = true;
                else opc = UtilDate.validateTimeFromString(date);
            }

            DateTime dateTime = DateTime.Now;
            if (date.Length > 0) dateTime = UtilDate.getTimeFromString(date);

            Console.Write("\n****************************************************************\n");
            Console.Write("****   Please enter the Duration of the Appointment:   *********\n");
            Console.Write("****************************************************************\n");
            int duration = 0;
            while (true)
            {
                String line = Console.ReadLine();
                if (Validator.isNumber(line))
                {
                    duration = int.Parse(line);
                    break;
                }
                Console.Write(">>Invalid Option: Insert a Number \n>>");
            }

            if(comment.Length > 0) aux.comment = comment;
            if(date.Length > 0) aux.date = dateTime;
            if(duration != aux.duration) aux.duration = duration;
            if(header.Length >0 ) aux.header = header;

            Manager.updateAppointment(aux);
        }

        public static void deleteAppointment()
        {
            //Console.Clear();
           // Console.Write(">> Locking other clients ...\n\n");
           // Manager.network.lockHosts(true);

    
                 Console.Write("****************************************************************\n");
                 Console.Write("*** Please enter the ID of the Appointment to be deleted:  *****\n");
                 Console.Write("****************************************************************\n");
                 if (!algTypeTR)
                 {
                     Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
                 }
                 else
                 {
                     Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
                 }


                 String idString = Console.ReadLine();
                 int inputConsole = 0;
                 try
                 {
                     inputConsole = int.Parse(idString);
                 }
                 catch
                 {
                     Console.Write(">>Invalid Option\n\n>>");
                 }
                 int id = inputConsole;
                 Appointment aux = Manager.getAppointmentById(id);
                 if (aux != null)
                 {
                     showAppointment(aux);

                     Boolean opc = false;
                     String confirmation = "";
                     while (!opc)
                     {
                         Console.Write("\n****************************************************************\n");
                         Console.Write("** Please confirm that you want to delete the appointment Y/N **\n");
                         Console.Write("****************************************************************\n");
                         confirmation = Console.ReadLine();
                         if (confirmation.Equals("Y") || confirmation.Equals("y") || confirmation.Equals("N") || confirmation.Equals("n"))
                         {
                             opc = true;
                         }
                     }

                     if (confirmation.Equals("Y") || confirmation.Equals("y"))
                     {
                         Manager.deleteAppointment(id);

                     }
                 }
           
        }

        public static Boolean connectMenu()
        {
            String url = "";
            string newHost;
            while(true){
                // Console.Clear();
                Console.Write("******************************************************************************\n");
                Console.Write("*** Please enter the URL of the server that you want to connect:         *****\n");
                Console.Write("*** TIP: The format must be:                                             *****\n");
                Console.Write("***  http:\\\\<server ip>:<serverport>\\<serviceName>.<serviceExtension> *****\n");
                Console.Write("******************************************************************************\n");
                Console.Write("***  To exit and work as a Standalone then press <<ENTER>>               *****\n");
                Console.Write("******************************************************************************\n\n");
                if (!algTypeTR)
                {
                    Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
                }
                else
                {
                    Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
                }
                url = Console.ReadLine();
                newHost = url;
                bool opc = Manager.network.connectHost(newHost); 
                if(opc){
                    break;
                }
                if (url.Length == 0){
                    break;
                }
            }

            if(url.Length != 0){
                Manager.setApointments(Manager.network.getAppointments(newHost));
                return true;
            }

            return false;
        }
    }
}
