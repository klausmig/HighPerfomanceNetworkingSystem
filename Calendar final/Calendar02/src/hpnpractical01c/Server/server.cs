using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Configuration;
using System.Runtime.Remoting.Channels.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using Microsoft.Samples.XmlRpc;
using Newtonsoft.Json;

public class AppointmentsServer : MarshalByRefObject, IAppointmentsManager
{

    public string portName = ConfigurationSettings.AppSettings["portName"];
    public string portNumber = ConfigurationSettings.AppSettings["portNumber"];
    public string portEndPoint = ConfigurationSettings.AppSettings["portEndPoint"];
    public string serverUrltoConnect = ConfigurationSettings.AppSettings["ServerUrl"];
    public string localIp = ConfigurationSettings.AppSettings["LocalIp"];
    public string algType = ConfigurationSettings.AppSettings["AlgorithmType"];

    public string ServerAddress = "", localIpAddress="";
    public Uri baseAddress;
    public ServiceHost selfHost;

    public AppointmentsServer()
    {
        // Step 1 Create a URI to serve as the base address.
        // baseAddress = new Uri("http://localhost:" + portNumber + "/AppointmentSystem/");
      // ServerAddress =  "http://localhost:" + portNumber + "/" + portName + "/";

       localIpAddress=Manager.network.getLocalIp ();

       //Manager.network.getLocalHost();
       ServerAddress = "http://" + localIp + ":" + portNumber + "/" + portName + "/";
       baseAddress = new Uri(ServerAddress);

       ServerAddress  += portEndPoint;

       if ((serverUrltoConnect !=null)&&(serverUrltoConnect != "")&&!( Manager.network.state))
       {
           Manager.network.connectHost(serverUrltoConnect);
           Manager.setApointments(Manager.network.getAppointments(serverUrltoConnect));
       }
      
        //baseAddress = new UriBuilder(Uri.UriSchemeHttp, Environment.MachineName, -1, "/AppointmentSystem/").Uri;
        // Step 2 Create a ServiceHost instance
        //selfHost = new ServiceHost(typeof(AppointmentsServer), baseAddress);
    }

    public Appointment[] GetAppointments()
    {
       // Console.WriteLine("New {0}", Manager.getAppointments().assigntments.Count);
        Appointments apos = Manager.getAppointments();
        return apos.assigntments.ToArray<Appointment>();
    }
    public string GetAppointmentsList()
    {
        Appointments apos = Manager.getAppointments();
        return JsonConvert.SerializeObject(apos.assigntments);
    }

    public bool SetAppointments(Appointment[] appointments)
    {
        List<Appointment> appointmentsList = appointments.ToList<Appointment>();
        Appointments aux = new Appointments();
        aux.assigntments = appointmentsList;
        return Manager.setApointments(aux);
    }

    public bool SetAppointmentslist(String appointmentsJson)
    {
        //Console.WriteLine(appointmentsJson);
        List<Appointment> appointmentsList = JsonConvert.DeserializeObject<List<Appointment>>(appointmentsJson);
      
        Appointments aux = new Appointments();
        aux.assigntments = appointmentsList;
        return Manager.setApointments(aux);
    }

    public bool test()
    {

        return true;
    }


    public bool GetConnection()
    {
        return true;
    }

    public Appointment GetAppointmentById(int id)
    {
        return Manager.getAppointmentById(id);
    }

    public void DeleteAppointment(Appointment appointment)
    {
        Manager.deleteAppointment(appointment);
    }
    
    public string[] GetHosts()
    {
        return Manager.network.hosts.ToArray<string>();
    }

    public string GetHostsString()
    {
        return JsonConvert.SerializeObject(Manager.network.hosts.ToArray<string>());
    }

    //Pass the token to next host >--------------------------------->
    public bool GiveToken()
    {
        TokenManager.tokenArrival();
        return true;
    }

    //Checking if is the node with the token >--------------------------------->//testing
    public bool IsTokenNode()
    {
        return TokenManager.haveTheToken;
    }

    //REMOVE HOSTS!!!!!! >--------------------------------->
    //public bool RemoveHost(string host)
    //{
    //    Manager.network.hosts.Remove(host);
    //    TokenManager.calculateNextHost();
    //    return true;
    //}

    //register a new host th the network supporting LC (logic clock)
    public int RegisterHost(string host)  //EDITED FOR R&A**********
    {
        Manager.network.algRicart.LC +=1; // recieved register host event 
        if (Manager.network.algRicart.LC == 1) Manager.network.algRicart.LC += 1;

        int LC = Manager.network.algRicart.LC;
        Console.Write("\n>>Client " + host + " connected. \n");
        if(algType != "tr")
        {
            Console.WriteLine("\n>>LC: {0} , LCClient:1\n ", Manager.network.algRicart.LC);
        }
        

        if (Manager.network.hosts.Count == 0) {
            Manager.network.hosts.Add(Manager.network.getLocalHost());
        }
       
        

        Manager.network.state = true;
        Manager.network.algRicart.LC ++;  // send reply event the LC value of the server

        foreach (string hostSaved in Manager.network.hosts){
            if (host == hostSaved) return Manager.network.algRicart.LC;
        //Console.Write("Salio");
        }

        Manager.network.hosts.Add(host);
        if (algType != "tr")
        {
            Console.Write("LC:" + Manager.network.algRicart.LC + ">>");
        }
        else
        {
            Console.Write("Hosts:" + Manager.network.hosts.Count + ">>");
        }
        
        return Manager.network.algRicart.LC;
    }

    public bool SetHosts(string[] hosts)
    {
        Manager.network.hosts = hosts.ToList<string>();
        return true;
    }

    public bool SetHostsString(string hosts)
    {
        List<string> hostsList = JsonConvert.DeserializeObject<List<string>>(hosts);
        Manager.network.hosts = hostsList;
        return true;
    }

    /// <summary>
    /// Method to init the server.
    /// </summary>
    public void RunServer()
    {        
        try
        {           
           // Console.WriteLine("XML-RPC Blog Demo");
            selfHost = new ServiceHost(typeof(AppointmentsServer));
            var epXmlRpc = selfHost.AddServiceEndpoint(typeof(IAppointmentsManager), new WebHttpBinding(WebHttpSecurityMode.None), new Uri(baseAddress, "./" + portEndPoint));
            epXmlRpc.Behaviors.Add(new XmlRpcEndpointBehavior());

            selfHost.Open();
        }
        catch (CommunicationException ce)
        {
            Console.WriteLine("An exception occurred: {0}", ce.Message);
            selfHost.Abort();
        }
        catch (Exception ce)
        {
            Console.WriteLine("An exception occurred: {0}", ce.Message);
            selfHost.Abort();
        }
    }

    /// <summary>
    /// Stop the running server
    /// </summary>
    public void closeServer()
    {
        //Manager.network.removeHost(Manager.network.getLocalHost());
        selfHost.Close();

    }

    /// <summary>
    /// Accept a request resource from a client host , answer ok if ther server host in not interested in the resource or send a lc if its interested

    public String requestResource(String messageClient)
    {

        string[] message = messageClient.Split(',');
        int LCClient =Convert.ToInt32(message[0]);
        string hostClient = message[1];

        
        if (Manager.network.algRicart.alreadyRequestResource) {

            if (Manager.network.algRicart.LCRequest< LCClient)
            {

                
                Console.WriteLine("\n>> This Host has priority, lesser LC");
                Console.WriteLine(">>LCRequest: {0} , LCClient:{1}, Client:{2}\n ", Manager.network.algRicart.LCRequest, LCClient, hostClient);
                Manager.network.algRicart.LC = Math.Max(LCClient, Manager.network.algRicart.LC) + 1;


                if (!(Manager.network.algRicart.requestQueue.Contains(hostClient)))
                {

                    Manager.network.algRicart.requestQueue.Add(hostClient);
                    Console.WriteLine(">>host:{0} added to requestQueue\n ", hostClient);
                }


                Manager.network.algRicart.LC++;
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");

                return Manager.network.algRicart.LC + "," +Manager.network.getLocalHost();
            }
            else 
            {
                Console.WriteLine("\n>>This Host has no priority, greater LC");
                Console.WriteLine(">>LCRequest: {0} , LCClient:{1}, Client:{2}\n ", Manager.network.algRicart.LCRequest, LCClient, hostClient);
                Manager.network.algRicart.LC = Math.Max(LCClient, Manager.network.algRicart.LC) + 1;

                if (!(Manager.network.algRicart.replyIPNotOk.Contains(hostClient)))
                {
                    Manager.network.algRicart.replyIPNotOk.Add(hostClient);
                    Console.WriteLine(">>host:{0} added to replyIPNotOk\n ", hostClient);
                }
                
                Manager.network.algRicart.LC++;
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");

                return Manager.network.algRicart.LC + "," + "ok";
            
            }
        
        
        }


        if (Manager.network.algRicart.usingResource)
        {
            Console.WriteLine("\n>>This Host is using the resource.");
            Manager.network.algRicart.LC = Math.Max(LCClient, Manager.network.algRicart.LC) + 1;
            Console.WriteLine(">>LC: {0} , LCClient:{1}, Client:{2}\n ", Manager.network.algRicart.LC, LCClient, hostClient);

            if (!(Manager.network.algRicart.requestQueue.Contains(hostClient)))
            {
                Manager.network.algRicart.requestQueue.Add(hostClient);
            
                Console.WriteLine(">>host:{0} added to requestQueue\n ", hostClient);
            }

            Manager.network.algRicart.LC++;
            Console.Write("LC:" + Manager.network.algRicart.LC + ">>");

            return Manager.network.algRicart.LC + "," + Manager.network.getLocalHost();
        }

        Console.WriteLine("\n>> This Host no needs the resource editing sending ok.\n");
        Manager.network.algRicart.LC = Math.Max(LCClient, Manager.network.algRicart.LC) + 1;

        Manager.network.algRicart.LC++;
        Console.Write("LC:" + Manager.network.algRicart.LC + ">>");

         
        return Manager.network.algRicart.LC + "," + "ok";
   


    }


    /// Accept a reply release ok from client host 

    public int replyOK(String messageClient)
    {

        string[] message = messageClient.Split(',');
        int LCClient = Convert.ToInt32(message[0]);
        String hostClient = message[1];


        if (Manager.network.algRicart.alreadyRequestResource)
        {

                Manager.network.algRicart.replyIPNotOk.Remove(hostClient);
                Console.WriteLine("\n>> Accepting Ok release from {0}.",hostClient);

                Manager.network.algRicart.LC = Math.Max(LCClient, Manager.network.algRicart.LC) + 1;
                Console.WriteLine(">>LC: {0} , LCClient:{1}\n ", Manager.network.algRicart.LC, LCClient);
                Console.Write("LC:" + Manager.network.algRicart.LC + ">>");

        }

        if ((Manager.network.algRicart.alreadyRequestResource) && (Manager.network.algRicart.replyIPNotOk.Count() == 0))
        { // all ok from nodes are recieved , changing state to access the resource

             Console.WriteLine("\n>> You have  the edit or delete resource, you can proceed !\n");
             Manager.network.algRicart.usingResource=true;
             Manager.network.algRicart.alreadyRequestResource=false;
        }

        return Manager.network.algRicart.LC++;

    }



}

