﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HPNPractical01C.Utils
{
    public static class UtilDate
    {
        public static DateTime getTimeFromString(string inputString){
            string[] dateTime = inputString.Split(' ');
            string[] date = dateTime[0].Split('.');
            string[] time = dateTime[1].Split(':');
            return new DateTime(int.Parse(date[0]), int.Parse(date[1]), int.Parse(date[2]), int.Parse(time[0]), int.Parse(time[1]),0);
        }

        public static Boolean validateTimeFromString(string inputString)
        {
            string[] dateTime = inputString.Split(' ');

            if (dateTime.Length == 2) { 
                string[] aux = dateTime[0].Split('.');
                int value;

                if (aux[0].Length != 4)
                {
                    Console.WriteLine("The Year is not in format YYYY");
                    return false;
                }
                if(!int.TryParse(aux[0], out value))
                {
                    Console.WriteLine("The Year is not an Integer");
                    return false;
                }
                if (aux[1].Length != 2)
                {
                    Console.WriteLine("The Month is not in format MM");
                    return false;
                }
                if (!int.TryParse(aux[1], out value))
                {
                    Console.WriteLine("The Month is not an Integer");
                    return false;
                }
                if (aux[2].Length != 2)
                {
                    Console.WriteLine("The Day is not in format DD");
                    return false;
                }
                if (!int.TryParse(aux[2], out value))
                {
                    Console.WriteLine("The Day is not an Integer");
                    return false;
                }

                string[] aux2 = dateTime[1].Split(':');
                if (aux2[0].Length != 2)
                {
                    Console.WriteLine("The Hour is not in format HH");
                    return false;
                }
                if (!int.TryParse(aux2[0], out value))
                {
                    Console.WriteLine("The Hour is not an Integer");
                    return false;
                }
                if (aux2[1].Length != 2)
                {
                    Console.WriteLine("The Minutes is not in format MM");
                    return false;
                }
                if (!int.TryParse(aux2[1], out value))
                {
                    Console.WriteLine("The Minutes is not an Integer");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("The Date is not in the correct Format");
                return false;
            }
            return true;
        }
    }
}
