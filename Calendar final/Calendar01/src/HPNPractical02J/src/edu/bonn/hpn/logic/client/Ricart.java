/**
 * 
 */
package edu.bonn.hpn.logic.client;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import edu.bonn.hpn.logic.Manager;

/**
 * @author Carlos
 * 
 */
public class Ricart {

	// Logic Clock
	public int LC = 0;
	public int LCRequest = 0;
	public List<String> requestQueue, requestIPServer;

	// list of ipadress denoates the nodes with reply not ok, only with all
	// nodes replying ok the actual node will accesss the resource.
	public List<String> replyIPNotOk;
	// Dictionary<string, bool> replyIPNotOk;

	private boolean alreadyRequestResource = false, usingResource = false;

	public Ricart() {
		setAlreadyRequestResource(false);
		setUsingResource(false);
	}

	// Request resource to all online nodes in hosts list.

	/**
	 * 
	 * @param hosts
	 */
	public void requestResource(List<String> hosts) {
		replyIPNotOk = new ArrayList<String>();
		requestQueue = new ArrayList<String>();

		System.out.println("\n>>Requesting Editing Appointment Resource to all nodes  ... ");

		String localHost = Manager.getNetwork().getLocalHost();
		List<String> hostsNew = new ArrayList<String>();

		for (String host : hosts) {

			try {
				if (!localHost.equals(host)) {
					XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

					config.setServerURL(new URL(host));
					config.setEnabledForExtensions(true);

					XmlRpcClient client = new XmlRpcClient();
					client.setConfig(config);
					Object[] params = new Object[] {};
					boolean connected = (boolean) client.execute(
							"appointments.GetConnection", params);

					if (connected) {
						LC++;
						LCRequest = LC;
						String parameter = LC + "," + localHost;
						params = new Object[] { parameter };
						String msg = (String) client.execute(
								"appointments.requestResource", params);

						System.out.println("requestResource msg reply:" + msg
								+ " from " + host);

						String[] message = msg.split(",");

						// Console.WriteLine(message.ToString());
						// LC = Math.Max( Convert.ToInt32(message[0]),LC) + 1;

						LC = Math.max(Integer.parseInt(message[0]), LC) + 1;

						// if (message[1] != "ok")
						if (!(message[1].equals("ok"))) {
							System.out.println("\n  Host {" + host
									+ "} replied NotOK replyIPNotOk: ");
							for (String url : replyIPNotOk) {
								System.out.println("\n" + url);
							}
							if (!(replyIPNotOk.contains(host)))
								replyIPNotOk.add(host); // Some Node has less LC
														// than this node
						} else {
							//System.out.println("\n  Host {" + host
								//	+ "} replied OK requestQueue: ");
							for (String url : requestQueue) {
								System.out.println("\n" + url);
							}

							if (!(requestQueue.contains(host))) {
								requestQueue.add(host); // Add nodes replied ok
														// so they have more lC
														// and are in the list
								System.out.println("\n  Host {" + host
										+ "} replied OK ");
							}
						}
						hostsNew.add(host);
					}
				}
			} catch (Exception e) {
				System.out.println("\n>>Error Connecting to host:" + host
						+ " ... Deleting ...\n");
			}
		}
		hostsNew.add(localHost);
		hosts = hostsNew;

		setAlreadyRequestResource(true);

		if (replyIPNotOk.size() > 0) {
			System.out.println("\n>Waiting ok reply from hosts:\n "); 
			for (String host : replyIPNotOk) { 
				System.out.println("\n>> "+host+" \n ");
			} 
		} else {		  
			usingResource = true; 
			alreadyRequestResource = false; 
		}
		
		/*if (replyIPNotOk.size() == 0) {
			setUsingResource(true);
			setAlreadyRequestResource(false);
		}*/
	}

	/**
	 * When finish using the resource reply Ok to all nodes in the list
	 */
	public void replyOK() {

		System.out
				.println("\n>>Replying Ok release to all nodes in the list nodes  ... ");
		String localHost = Manager.getNetwork().getLocalHost();
		List<String> hostsNew = new ArrayList<String>();

		for (String host : requestQueue) {
			try {
				if (!localHost.equals(host)) {
					XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

					config.setServerURL(new URL(host));
					config.setEnabledForExtensions(true);

					XmlRpcClient client = new XmlRpcClient();
					client.setConfig(config);
					Object[] params = new Object[] {};
					boolean connected = (boolean) client.execute(
							"appointments.GetConnection", params);

					if (connected) {
						LC++;
						String parameter = LC + "," + localHost;
						params = new Object[] { parameter };
						
						int LCClient = (int)client.execute("appointments.replyOK", params);
						
						LC = Math.max(LCClient, LC) + 1;
						System.out.println("\n  to Host {" + host + "} sended OK ");
						hostsNew.add(host);
					}
				}
			} catch (Exception e) {
				System.out.println(">>Error Connecting to host:" + host
						+ " ... Deleting ...\n");
			}
		}

		System.out.println("\n");
		setAlreadyRequestResource(false);
		setUsingResource(false);
	}

	/**
	 * @return the alreadyRequestResource
	 */
	public boolean isAlreadyRequestResource() {
		return alreadyRequestResource;
	}

	/**
	 * @param alreadyRequestResource
	 *            the alreadyRequestResource to set
	 */
	public void setAlreadyRequestResource(boolean alreadyRequestResource) {
		this.alreadyRequestResource = alreadyRequestResource;
	}

	/**
	 * @return the usingResource
	 */
	public boolean isUsingResource() {
		return usingResource;
	}

	/**
	 * @param usingResource
	 *            the usingResource to set
	 */
	public void setUsingResource(boolean usingResource) {
		this.usingResource = usingResource;
	}

	
	/**
	 * This method cleans the servers those we are waiting for a ok response,
	 * sometimes some servers are down recover algorithm Ricart
	 */
	public void CleanReplyNotOkList() {

		System.out
				.println("\n>>Updating response Ok from Host list notOk ...\n ");
		String localHost = Manager.getNetwork().getLocalHost();

		List<String> hostsNew = new ArrayList<String>();

		for (String host : replyIPNotOk) {
			try {
				if (!localHost.equals(host)) {
					XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

					config.setServerURL(new URL(host));
					config.setEnabledForExtensions(true);

					XmlRpcClient client = new XmlRpcClient();
					client.setConfig(config);
					Object[] params = new Object[] {};
					boolean connected = (boolean) client.execute(
							"appointments.GetConnection", params);

					if (connected) {
						hostsNew.add(host);
					}
				}
			} catch (Exception e) {
				System.out.println(">>Error Connecting to host:" + host
						+ " ... Deleting ...\n");
				// hosts.Remove(host);
			}
			//hostsNew.add(localHost);
			replyIPNotOk = hostsNew;
		}
	}
}
