/**
 * 
 */
package edu.bonn.hpn.logic;

import java.util.Properties;

import edu.bonn.hpn.entities.Appointment;
import edu.bonn.hpn.entities.Appointments;
import edu.bonn.hpn.entities.perstistence.Persistence;
import edu.bonn.hpn.logic.client.Network;
import edu.bonn.hpn.util.IConstantsValues;
import edu.bonn.hpn.util.PropsUtils;

/**
 * This class as is call, is the one that manage the system of appointments
 * @author Carlos
 * 
 */
public class Manager {
	/**
	 * Static attributes of the class
	 */
	private static Manager singleton = new Manager();
	private static Network network = new Network();
	private Properties configProp = new Properties();
	private String sName;
	
	/*
	 * A private Constructor prevents any other class from instantiating.
	 */
	private Manager() {
		loadProps();
		setsName(configProp.getProperty(IConstantsValues.FILE_NAME));
	}

	/* Static 'instance' method */
	public static Manager getInstance() {
		return singleton;
	}

	/**
	 * 
	 * @return
	 */
	public static Appointments getAppointments() {
		return Persistence.getInstance().getAppointments();
	}

	/**
	 * This Methos get the last id in the system.
	 * 
	 * @return
	 */
	public static int getLastId() {
		Appointments aux = getAppointments();
		int highId = 0;
		// There is pending the implementation with the sincronization between
		// servers.
		for (Appointment a : aux.getAppointments()) {
			if (a.getId() > highId) {
				highId = a.getId();
			}
		}
		highId++;
		return highId;
	}

	/**
	 * 
	 * @param newAppointment
	 */
	public static void saveAppointment(Appointment newAppointment) {
		Appointments aux = Manager.getAppointments();
		aux.getAppointments().add(newAppointment);
		Persistence.getInstance().saveAppointments(aux);

		//network.lockHosts(true);
		network.SynchronizeAppointment(aux);
		//network.lockHosts(false);
	}

	/**
	 * This method save an appointment into the system.
	 */
	private void loadProps() {
		try {
			setConfigProp(PropsUtils.load(IConstantsValues.CONFIG_FILE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method return an appointment given an ID
	 * @param id
	 * @return
	 */
	public static Appointment getAppointmentById(int id)
    {
        Appointments aux = Manager.getAppointments();
        for (Appointment a: aux.getAppointments())
        {
            if (a.getId() == id)
            {
                return a;
            }
        }
        return null;
    }
	
	/**
	 * This method update
	 * @param appointment
	 */
	public static void updateAppointment(Appointment appointment)
    {
        Appointments aux = Manager.getAppointments();
        for (Appointment a : aux.getAppointments())
        {
            if (a.getId() == appointment.getId())
            {
                a.setComment(appointment.getComment());
                a.setDate(appointment.getDate());
                a.setDuration(appointment.getDuration());
                a.setHeader(appointment.getHeader());
            }
        }
        Persistence.getInstance().saveAppointments(aux);

        //network.lockHosts(true);
        network.SynchronizeAppointment(aux);
        //network.lockHosts(false);

    }
	
	/**
	 * 
	 * @param id
	 */
	public static void deleteAppointment(int id)
    {
        Appointments aux = Manager.getAppointments();
        Appointment auxDel = new Appointment();
        for(Appointment a : aux.getAppointments())
        {
            if (a.getId() == id)
            {
                auxDel = a;
            }
        }
        aux.getAppointments().remove(auxDel);
        Persistence.getInstance().saveAppointments(aux);

        //network.lockHosts(true);
        network.SynchronizeAppointment(aux);
        //network.lockHosts(false);
    }
	
	/**
	 * 
	 * @param appointment
	 */
	public static void deleteAppointment(Appointment appointment)
    {
        Appointments aux = Manager.getAppointments();
        aux.getAppointments().remove(appointment);
        Persistence.getInstance().saveAppointments(aux);

        //network.lockHosts(true);
        network.SynchronizeAppointment(aux);
        //network.lockHosts(false);
    }
	
	/**
	 * 
	 * @param appointments
	 * @return
	 */
	public static boolean setApointments(Appointments appointments)
    {
        try 
        { 
        	if(appointments != null){
        		Persistence.getInstance().saveAppointments(appointments);
        	}
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
	
	/**
	 * @return the configProp
	 */
	public Properties getConfigProp() {
		return configProp;
	}

	/**
	 * @param configProp
	 *            the configProp to set
	 */
	public void setConfigProp(Properties configProp) {
		this.configProp = configProp;
	}

	/**
	 * @return the network
	 */
	public static Network getNetwork() {
		return network;
	}

	/**
	 * @param network
	 *            the network to set
	 */
	public static void setNetwork(Network network) {
		Manager.network = network;
	}

	/**
	 * @return the sName
	 */
	public String getsName() {
		return sName;
	}

	/**
	 * @param sName
	 *            the sName to set
	 */
	public void setsName(String sName) {
		this.sName = sName;
	}
	
}
