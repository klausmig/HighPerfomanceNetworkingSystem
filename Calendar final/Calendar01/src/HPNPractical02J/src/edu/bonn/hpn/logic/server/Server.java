package edu.bonn.hpn.logic.server;

import java.util.Properties;

import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import edu.bonn.hpn.logic.Manager;
import edu.bonn.hpn.util.IConstantsValues;
import edu.bonn.hpn.util.PropsUtils;

/**
 * @author Carlos
 *
 */
public class Server {

	private WebServer webServer;
	private Properties configProp = new Properties();

	public String ServerAddress = "", localIp = "";
    public String serverUrltoConnect = "";

	/**
	 * 
	 */
	public Server() {
		this.loadProps();
		localIp = Manager.getNetwork().getLocalIp();
		Object port = configProp.getProperty(IConstantsValues.PORT);
		String portStr = (String) port;
		Object url = configProp.getProperty(IConstantsValues.URL_TO_CONNECT);
		serverUrltoConnect = (String) url;
		
		ServerAddress = "http://" + localIp + ":" + portStr + "/xmlrpc";
		
		 if ((serverUrltoConnect !=null)&&(!serverUrltoConnect.isEmpty())&&!( Manager.getNetwork().isState()))
	       {
	           Manager.getNetwork().connectHost(serverUrltoConnect);
	           Manager.setApointments(Manager.getNetwork().getAppointments(serverUrltoConnect));
	       }
		
	}

	/**
	 * 
	 */
	public void run() {
		try {

			Object port = configProp.getProperty(IConstantsValues.PORT);
			String portStr = (String) port;
			int portInt = Integer.parseInt(portStr);
			webServer = new WebServer(portInt);

			XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();

			PropertyHandlerMapping phm = new PropertyHandlerMapping();
			phm.addHandler("appointments", ServerFunctions.class);
			xmlRpcServer.setHandlerMapping(phm);

			XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer
					.getConfig();
			serverConfig.setEnabledForExtensions(true);
			serverConfig.setContentLengthOptional(false);

			webServer.start();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * 
	 */
	public void stop() {
		webServer.shutdown();
	}

	/**
	 * 
	 */
	public void loadProps() {
		try {
			configProp = PropsUtils.load(IConstantsValues.CONFIG_FILE_NAME);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}