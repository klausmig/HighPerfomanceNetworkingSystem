/**
 * 
 */
package edu.bonn.hpn.util;

public class Validator {

	@SuppressWarnings("unused")
	public static boolean isNumber(String str) {
		try {
			int num = Integer.parseInt(str);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
