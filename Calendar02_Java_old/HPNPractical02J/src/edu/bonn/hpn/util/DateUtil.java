package edu.bonn.hpn.util;

import java.util.Date;

public class DateUtil {

	@SuppressWarnings("deprecation")
	public static String getStringDate(Date in){
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(in.getDate());
		stringBuilder.append("-");
		stringBuilder.append(in.getMonth()+1);
		stringBuilder.append("-");
		stringBuilder.append(in.getYear()+1900);
		stringBuilder.append(" ");
		stringBuilder.append(in.getHours());
		stringBuilder.append(":");
		stringBuilder.append(in.getMinutes());
		stringBuilder.append(":");
		stringBuilder.append(in.getSeconds());
		return stringBuilder.toString();
	}	
	
	public static Date getTimeFromString(String inputString){
		String[] dateTime = inputString.split(" ");
		String[] date = dateTime[0].split("\\.");
		String[] time = dateTime[1].split(":");
        return new Date(Integer.parseInt(date[0])-1900, Integer.parseInt(date[1]), Integer.parseInt(date[2]), Integer.parseInt(time[0]), Integer.parseInt(time[1]),0);
    }


	/**
	 * 2012-12-12T12:00:00
	 * @param inputString
	 * @return
	 */
	public static Date getTimeFromString2(String inputString){
		String[] dateTime = inputString.split("T");
		String[] date = dateTime[0].split("-");
		String[] time = dateTime[1].split(":");
		return new Date(Integer.parseInt(date[0])-1900, Integer.parseInt(date[1]), Integer.parseInt(date[2]), Integer.parseInt(time[0]), Integer.parseInt(time[1]),0);
	}
	
    public static boolean validateTimeFromString(String inputString)
    {
        String[] dateTime = inputString.split(" ");

        if (dateTime.length == 2) { 
        	String variable1 = dateTime[0];
            String[] aux = variable1.split("\\.");
            int value;

            if (aux[0].length() != 4)
            {
                System.out.println("The Year is not in format YYYY");
                return false;
            }            
            if (aux[1].length() != 2)
            {
                System.out.println("The Month is not in format MM");
                return false;
            }
            if (aux[2].length() != 2)
            {
                System.out.println("The Day is not in format DD");
                return false;
            }

            String[] aux2 = dateTime[1].split(":");
            if (aux2[0].length() != 2)
            {
                System.out.println("The Hour is not in format HH");
                return false;
            }
            if (aux2[1].length() != 2)
            {
                System.out.println("The Minutes is not in format MM");
                return false;
            }
        }
        else
        {
            System.out.println("The Date is not in the correct Format");
            return false;
        }
        return true;
    }
	
}
