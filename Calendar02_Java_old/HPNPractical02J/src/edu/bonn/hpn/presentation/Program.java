package edu.bonn.hpn.presentation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;

import edu.bonn.hpn.entities.Appointment;
import edu.bonn.hpn.entities.Appointments;
import edu.bonn.hpn.logic.Manager;
import edu.bonn.hpn.logic.client.TokenManager;
import edu.bonn.hpn.logic.server.Server;
import edu.bonn.hpn.util.DateUtil;
import edu.bonn.hpn.util.IConstantsValues;
import edu.bonn.hpn.util.Validator;

public class Program {

	private static Server server = new Server();
	static BufferedReader console = new BufferedReader(new InputStreamReader(
			System.in));
	
	public static boolean algTypeTR = false;

	/**
	 * Main program
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// Initialize the server to be able to work as a server
		server = new Server();
		server.run();
		
		//Algorithm type definition ----------------------------------->
		if(Manager.getNetwork().getConfigProp().getProperty(IConstantsValues.ALG_TYPE).equalsIgnoreCase("tr")){ //Checking algorithm type
			algTypeTR = true;
		}
		
		//Starting token for 1st time and starting thread          ----------------------------------->
		if (algTypeTR){
            if (Manager.getNetwork().getHosts().size() <= 1){
                TokenManager.haveTheToken = true;
                TokenManager.startTimer();
            }else {
            	TokenManager.startTimer();
            }
        }
		
		int menuOptHom = 0; // Option Menu Home
		int menuOptApp = 0; // Option Menu Appointments
		int menuOptNet = 0; // Option Menu Network

		while (menuOptHom != 4) {// Home Menu

			menuOptHom = 0; // Option Menu Home
			menuOptApp = 0; // Option Menu Appointments
			menuOptNet = 0; // Option Menu Network

			menuOptHom = InitMenu();

			if (menuOptHom == 1) {// Option to Manage the Appointments

				while (menuOptApp != 5) {

					menuOptApp = AppointmentsMenu();

					if (menuOptApp == 2 || menuOptApp == 3 || (menuOptApp == 1)) {
						if (algTypeTR) { // Starting with Token Ring Algorithm      ----------------------------------->
							if (!TokenManager.alreadyRequestToken && !TokenManager.haveTheToken)
                            {
                                TokenManager.alreadyRequestToken = true;
                                System.out.println("\n> Waiting for token... ");
                            }
                            else if (TokenManager.alreadyRequestToken && !TokenManager.haveTheToken)
                            {
                            	System.out.println("\n> Still waiting for token... ");
                            }
                            else
                            {
                                TokenManager.usingToken = true;
                                TokenManager.alreadyRequestToken = false;
                            }
						} else { // Starting with R&A Algorithm
							if (!Manager.getNetwork().getAlgRicart().isAlreadyRequestResource() && !Manager.getNetwork().getAlgRicart().isUsingResource()) {
								Manager.getNetwork().getAlgRicart().requestResource(
								Manager.getNetwork().getHosts());
							} else if (Manager.getNetwork().getAlgRicart().isAlreadyRequestResource() && !Manager.getNetwork().getAlgRicart().isUsingResource()) {
								Manager.getNetwork().getAlgRicart().CleanReplyNotOkList();
								if (Manager.getNetwork().getAlgRicart().replyIPNotOk.size() > 0) {
									System.out.println("\n>Accesing resource .... waiting ok reply from hosts: ");
									for (String host : Manager.getNetwork().getAlgRicart().replyIPNotOk) {
										System.out.println(">>" + host);
									}
									System.out.println("");
								}else{
									Manager.getNetwork().getAlgRicart().setAlreadyRequestResource(false);
									Manager.getNetwork().getAlgRicart().setUsingResource(true);
								}
							}
						}
					}
					if ((menuOptApp == 1) && Manager.getNetwork().getAlgRicart().isUsingResource() && !algTypeTR) 
                    {
                        createAppointmentMenu();
                        Manager.getNetwork().getAlgRicart().replyOK(); //Release the resource
                    } else if ((menuOptApp == 1) && TokenManager.haveTheToken && algTypeTR) // TOKEN RING CODE ---------------------------->
                    {
                        createAppointmentMenu();
                        TokenManager.passToken(); //passig the token
                    } else if ((menuOptApp == 2) && Manager.getNetwork().getAlgRicart().isUsingResource() && !algTypeTR) 
                    {
						deleteAppointment();
						Manager.getNetwork().getAlgRicart().replyOK();
					} else if((menuOptApp == 2) && TokenManager.haveTheToken && algTypeTR) // TOKEN RING CODE ---------------------------->
					{ 
						deleteAppointment();
						TokenManager.passToken(); //passing the token
					} else if ((menuOptApp == 3) && Manager.getNetwork().getAlgRicart().isUsingResource() && !algTypeTR) 
					{
						modifyAppointment();
						Manager.getNetwork().getAlgRicart().replyOK();
					}else if((menuOptApp == 3) && TokenManager.haveTheToken && algTypeTR)// TOKEN RING CODE ---------------------------->
					{
						modifyAppointment();
                        TokenManager.passToken(); //passing the token
					} else if (menuOptApp == 4) 
					{
						listAppointmentsMenu();
					} else if (menuOptApp == 6) 
					{
						menuOptApp = 5;
						menuOptHom = 4;
					}
				}
			} else if (menuOptHom == 2) {
				while (menuOptNet != 4) {
					menuOptNet = NetworkMenu();
					if (menuOptNet == 1) {
						if (connectMenu()) 
						{
							Manager.getNetwork().setState(true);
						} else 
						{
							Manager.getNetwork().setState(false);
						}
					} else if (menuOptNet == 2) {
						if (TokenManager.haveTheToken && Manager.getNetwork().getHosts().size() > 1) // TOKEN RING CODE ---------------------------->
                        {
                            TokenManager.passToken();
                        }
						if (algTypeTR){
							Manager.getNetwork().removeHost(Manager.getNetwork().getLocalHost());
							Manager.getNetwork().SynchronizeHost();
							Manager.getNetwork().setState(false);
						} else {
							Manager.getNetwork().setState(false);
							Manager.getNetwork().removeHost(Manager.getNetwork().getLocalHost());
							Manager.getNetwork().SynchronizeHost();
							Manager.getNetwork().setHosts(new ArrayList<String>());
						}

					} else if (menuOptNet == 3) {

						for (String host : Manager.getNetwork().getHosts()) {
							System.out.println(host + "\n");

						}
					} else if (menuOptNet == 5) {
						menuOptNet = 4;
						menuOptHom = 4;
					}
				}
			} else if (menuOptHom == 3)// Exit
			{
				AboutMenu();
			}
		}
		if (TokenManager.haveTheToken == true && Manager.getNetwork().getHosts().size() > 1) // TOKEN RING CODE ---------------------------->
        {
            TokenManager.passToken();
        }
		if (algTypeTR) {
			//TokenManager.timer.cancel();
			Manager.getNetwork().removeHost(Manager.getNetwork().getLocalHost());
			Manager.getNetwork().SynchronizeHost();
		}
		server.stop();
		System.exit(0);
	}

	/**
	 * Init Menu
	 * 
	 * @throws IOException
	 */
	public static int InitMenu() throws IOException {
		// Console.Clear();
		System.out
				.println("****************************************************************");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************   HPN: Practical Assigntment 2    ***************");
		System.out
				.println("**************        JAVA Solution v0.0.1         *************");
		System.out
				.println("****************************************************************");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************     OPTIONS:                       **************");
		System.out
				.println("**************         1. Manage Appointments     **************");
		System.out
				.println("**************         2. Manage Network          **************");
		System.out
				.println("**************         3. About us                **************");
		System.out
				.println("**************         4. Exit                    **************");
		System.out
				.println("****************************************************************");
		System.out
				.println("****************************************************************");
		System.out.println("URL Server: " + server.ServerAddress);
		System.out.println("Number of hosts in the Network: " + Manager.getNetwork().getHosts().size());
		System.out
				.println("****************************************************************");
		if (!algTypeTR)
        {
			System.out.print("LC:" +Manager.getNetwork().getAlgRicart().LC + ">>");
        }
        else
        {
        	System.out.print("Hosts:"+Manager.getNetwork().getHosts().size()+">>");
        }

		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				return Integer.parseInt(line);
			}
			System.out.println(">>Invalid Option: Insert a Number >>");
		}
	}

	/**
	 * About Menu
	 * 
	 * @throws IOException
	 */
	public static void AboutMenu() throws IOException {
		// Console.Clear();
		System.out
				.println("****************************************************************");
		System.out
				.println("**************     GROUP MEMBERS:                 **************");
		System.out
				.println("**************         Cristobal Leiva            **************");
		System.out
				.println("**************         Klaus Martinez             **************");
		System.out
				.println("**************         Carlos Montoya             **************");
		System.out
				.println("**************         Ahmad Amayri               **************");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************   To continue press enter          **************");
		System.out
				.println("****************************************************************\n");
		if (!algTypeTR)
        {
			System.out.print("LC:" +Manager.getNetwork().getAlgRicart().LC + ">>");
        }
        else
        {
        	System.out.print("Hosts:"+Manager.getNetwork().getHosts().size()+">>");
        }

		console.readLine();
	}

	/**
	 * Appointment Menu
	 * 
	 * @throws IOException
	 */
	public static int AppointmentsMenu() throws IOException {
		// Console.Clear();
		System.out
				.println("****************************************************************");
		System.out
				.println("**************   Appointments Menu:               **************");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************     OPTIONS:                       **************");
		if (Manager.getNetwork().isState() && Manager.getNetwork().getHosts().size() > 1) {
			System.out
					.println("**************         1. Create Appointment      **************");
			System.out
					.println("**************         2. Delete Appointment      **************");
			System.out
					.println("**************         3. Modify Appointment      **************");
		} 
		System.out
				.println("**************         4. List Appointments       **************");
		System.out
				.println("**************         5. Return to Home          **************");
		System.out
				.println("**************         6. Exit                    **************");
		System.out
				.println("****************************************************************\n");
		if (!algTypeTR)
        {
			System.out.print("LC:" +Manager.getNetwork().getAlgRicart().LC + ">>");
        }
        else
        {
        	System.out.print("Hosts:"+Manager.getNetwork().getHosts().size()+">>");
        }

		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				return Integer.parseInt(line);
			}
			System.out.println(">>Invalid Option: Insert a Number \n>>");
		}
	}

	/**
	 * Networking Menu
	 * 
	 * @throws IOException
	 */
	public static int NetworkMenu() throws IOException {
		// Console.Clear();
		System.out
				.println("****************************************************************");
		System.out
				.println("**************   Network Menu:                    **************");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************     OPTIONS:                       **************");
		System.out
				.println("**************         1. Connect                 **************");
		System.out
				.println("**************         2. Disconnect              **************");
		System.out
				.println("**************         3. List Host               **************");
		System.out
				.println("**************         4. Return to Home          **************");
		System.out
				.println("**************         5. Exit                    **************");
		System.out
				.println("****************************************************************\n");
		if (!algTypeTR)
        {
			System.out.print("LC:" +Manager.getNetwork().getAlgRicart().LC + ">>");
        }
        else
        {
        	System.out.print("Hosts:"+Manager.getNetwork().getHosts().size()+">>");
        }

		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				return Integer.parseInt(line);
			}
			System.out.println(">>Invalid Option: Insert a Number \n>>");
		}
	}

	/**
	 * This method show all the appointments
	 * 
	 * @throws IOException
	 */
	public static void listAppointmentsMenu() throws IOException {
		Appointments list = new Appointments();
		list = Manager.getAppointments();

		System.out.println("\n\n\n\n");
		System.out
				.println("****************************************************************");
		System.out
				.println("**************         Appointments List:         **************");
		System.out
				.println("****************************************************************\n");
		if (list.getAppointments() != null && list.getAppointments().size() > 0) {
			for (Appointment a : list.getAppointments()) {
				System.out.println(a.toString() + "\n");
			}
		} else {
			System.out
					.println("**************  No Appointments in the system     **************\n");
		}

		System.out
				.println("****************************************************************");
		System.out
				.println("**************   To continue press enter          **************");
		System.out
				.println("****************************************************************\n");
		String line = console.readLine();
		// Console.SetWindowSize(origWidth, origHeight);
	}

	/**
	 * This method creates an Appointment
	 * 
	 * @throws IOException
	 */
	public static void createAppointmentMenu() throws IOException {
		System.out.println("\n");
		System.out
				.println("****************************************************************");
		System.out
				.println("****   Please enter the Header of the Appointment:    **********");
		System.out
				.println("****************************************************************");
		String header = console.readLine();

		System.out
				.println("\n****************************************************************");
		System.out
				.println("****   Please enter the Comments of the Appointment:   *********");
		System.out
				.println("****************************************************************");
		String comment = console.readLine();

		Boolean opc = false;
		String date = "";
		while (!opc) {
			System.out
					.println("\n****************************************************************");
			System.out
					.println("****   Please enter the Date of the Appointment           ******");
			System.out
					.println("****   The Date should have the format YYYY.MM.DD HH:MM   ******");
			System.out
					.println("****************************************************************");
			date = console.readLine();
			opc = DateUtil.validateTimeFromString(date);
		}

		Date dateTime = DateUtil.getTimeFromString(date);

		System.out
				.println("\n****************************************************************");
		System.out
				.println("****   Please enter the Duration of the Appointment:   *********");
		System.out
				.println("****************************************************************");
//		String durationString = console.readLine();
		int duration = 0;
		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				duration = Integer.parseInt(line);
				break;
			}
			System.out.print(">>Invalid Option: Insert a Number \n>>");
		}
		int id = Manager.getLastId();

		Appointment newAppointment = new Appointment();
		newAppointment.setId(id);
		newAppointment.setComment(comment);
		newAppointment.setDate(dateTime);
		newAppointment.setDuration(duration);
		newAppointment.setHeader(header);

		Manager.saveAppointment(newAppointment);
	}

	/*
	 * This Method show the info of an appointment
	 */
	public static void showAppointment(Appointment appointment) {
		System.out
				.println("\n****************************************************************");
		System.out.println("      Id: " + appointment.getId());
		System.out.println("  Header: " + appointment.getHeader());
		System.out.println(" Comment: " + appointment.getComment());
		System.out.println("    Date: " + appointment.getDate().toString());
		System.out.println("Duration: " + appointment.getDuration());
		System.out
				.println("****************************************************************\n");
	}

	/**
	 * This method modify and existing appointment
	 * 
	 * @throws IOException
	 */
	public static void modifyAppointment() throws IOException {
		System.out.println("\n");
		System.out
				.println("****************************************************************");
		System.out
				.println("****  Please enter the ID of the Appointment to consult:   *****");
		System.out
				.println("****************************************************************");
		//String idString = console.readLine();
		int id = 0;
		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				id = Integer.parseInt(line);
				break;
			}
			System.out.print(">>Invalid Option: Insert a Number \n>>");
		}
		Appointment aux = Manager.getAppointmentById(id);
		showAppointment(aux);

		System.out
				.println("\n****************************************************************");
		System.out
				.println("****   Please enter the Header of the Appointment:    **********");
		System.out
				.println("****************************************************************");
		String header = console.readLine();

		System.out
				.println("\n****************************************************************");
		System.out
				.println("****   Please enter the Comments of the Appointment:   *********");
		System.out
				.println("****************************************************************");
		String comment = console.readLine();

		Boolean opc = false;
		String date = "";
		while (!opc) {
			System.out
					.println("\n****************************************************************");
			System.out
					.println("****   Please enter the Date of the Appointment           ******");
			System.out
					.println("****   The Date should have the format YYYY.MM.DD HH:MM   ******");
			System.out
					.println("****************************************************************");
			date = console.readLine();
			if (date.length() == 0)
				opc = true;
			else
				opc = DateUtil.validateTimeFromString(date);
		}

		Date dateTime = new Date();
		if (date.length() > 0)
			dateTime = DateUtil.getTimeFromString(date);

		System.out
				.println("\n****************************************************************");
		System.out
				.println("****   Please enter the Duration of the Appointment:   *********");
		System.out
				.println("****************************************************************");
		int duration = 0;
		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				duration = Integer.parseInt(line);
				break;
			}
			System.out.print(">>Invalid Option: Insert a Number \n>>");
		}

		if (comment.length() > 0)
			aux.setComment(comment);
		if (date.length() > 0)
			aux.setDate(dateTime);
		if (duration != aux.getDuration())
			aux.setDuration(duration);
		if (header.length() > 0)
			aux.setHeader(header);

		Manager.updateAppointment(aux);
	}

	public static void deleteAppointment() throws IOException {
		System.out.println("\n");

		System.out
				.println("****************************************************************");
		System.out
				.println("*** Please enter the ID of the Appointment to be deleted:  *****");
		System.out
				.println("****************************************************************");
		int id = 0;
		while (true) {
			String line = console.readLine();
			if (Validator.isNumber(line)) {
				id = Integer.parseInt(line);
				break;
			}
			System.out.print(">>Invalid Option: Insert a Number \n>>");
		}
		Appointment aux = Manager.getAppointmentById(id);

		if (aux != null) {
			showAppointment(aux);

			Boolean opc = false;
			String confirmation = "";
			while (!opc) {
				System.out
						.println("\n****************************************************************");
				System.out
						.println("** Please confirm that you want to delete the appointment Y/N **");
				System.out
						.println("****************************************************************");
				confirmation = console.readLine();
				if (confirmation.equals("Y") || confirmation.equals("y")
						|| confirmation.equals("N") || confirmation.equals("n")) {
					opc = true;
				}
			}

			if (confirmation.equals("Y") || confirmation.equals("y")) {
				Manager.deleteAppointment(id);

			}
		}
	}

	public static Boolean connectMenu() throws IOException {
		String url = "";
		String newHost;
		while (true) {
			// Console.Clear();
			System.out
					.println("******************************************************************************");
			System.out
					.println("*** Please enter the URL of the server that you want to connect:         *****");
			System.out
					.println("*** TIP: The format must be:                                             *****");
			System.out
					.println("***  http:\\\\<server ip>:<serverport>\\<serviceName>.<serviceExtension> *****");
			System.out
					.println("******************************************************************************");
			System.out
					.println("***  To exit and work as a Standalone then press <<ENTER>>               *****");
			System.out
					.println("******************************************************************************\n");
			if (!algTypeTR)
	        {
				System.out.print("LC:" +Manager.getNetwork().getAlgRicart().LC + ">>");
	        }
	        else
	        {
	        	System.out.print("Hosts:"+Manager.getNetwork().getHosts().size()+">>");
	        }
			url = console.readLine();
			newHost = url;
			boolean opc = Manager.getNetwork().connectHost(newHost);
			if (opc) {
				break;
			}
			if (url.length() == 0) {
				break;
			}
		}

		if (url.length() != 0) {
			Manager.setApointments(Manager.getNetwork()
					.getAppointments(newHost));
			return true;
		}

		return false;
	}

}
