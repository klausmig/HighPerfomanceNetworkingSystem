/**
 * 
 */
package edu.bonn.hpn.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Carlos
 *
 */
public class Appointment implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8205302472130208686L;
	/**
	 * Attributes
	 */
	private int id;
	private Date date;
	private int duration;
	private String header;
	private String comment;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the adate
	 */
	public Date getDate() {
		return date;
	}
	/**
	 * @param adate the adate to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}
	/**
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * Class contructor
	 */
	public Appointment(){
		
	}
	
	/**
	 * toString funtion
	 */
	public String toString(){
		return "ID: " + this.id + " ,header: "+ this.header + " ,comment: " + this.comment + " ,Date: "+ this.date.toString() + " ,duration: " + this.duration;
	}
	
}
