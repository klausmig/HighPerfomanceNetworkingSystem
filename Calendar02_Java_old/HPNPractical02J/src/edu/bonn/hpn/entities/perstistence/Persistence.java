/**
 * 
 */
package edu.bonn.hpn.entities.perstistence;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import edu.bonn.hpn.entities.Appointment;
import edu.bonn.hpn.entities.Appointments;
import edu.bonn.hpn.util.IConstantsValues;

/**
 * @author Carlos
 * 
 */
public class Persistence {

	/**
	 * 
	 */
	private static Persistence instance = null;

	/**
	 * 
	 */
	protected Persistence() {
		// Exists only to defeat instantiation.
	}

	/**
	 * Give the singleton Persistence
	 * 
	 * @return
	 */
	public static Persistence getInstance() {
		if (instance == null) {
			instance = new Persistence();
		}
		return instance;
	}

	/**
	 * Save the entire collection of appointments
	 * 
	 * @param collection
	 *            Collection of Appointments to be store.
	 * @throws FileNotFoundException
	 */
	public void saveAppointments(Appointments collection) {
		try {
			FileOutputStream os = new FileOutputStream(
					IConstantsValues.FILE_NAME);

			XMLEncoder encoder = new XMLEncoder(os);
			encoder.writeObject(collection);
			encoder.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	/**
	 * Restore from the local file the arrange of appointments
	 * 
	 * @return The local appoinments
	 * @throws FileNotFoundException
	 */
	public Appointments getAppointments() {
		try {
			File f = new File(IConstantsValues.FILE_NAME);
			Appointments collection;
			if (f.exists()) {
				FileInputStream is = new FileInputStream(
						IConstantsValues.FILE_NAME);
				XMLDecoder decoder = new XMLDecoder(is);
				collection = (Appointments) decoder.readObject();
				decoder.close();
			} else {
				collection = new Appointments();
			}
			return collection;
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		return null;
	}

	/**
	 * This method store and unique appoinment into the system.
	 * 
	 * @param appointment
	 * @return true if was possible to store it, false if not
	 */
	public boolean saveAppointment(Appointment appointment) {
		Appointments collection = this.getAppointments();
		if (appointment.getId() != 0) {
			for (Appointment a : collection.getAppointments()) {
				if (a.getId() == appointment.getId()) {
					System.out.println("The id:" + a.getId() + " already exists in the system");
					return false;
				}
			}
		} else {
			int id = this.getNextID();
			appointment.setId(id);
		}

		collection.getAppointments().add(appointment);
		this.saveAppointments(collection);

		return true;
	}

	/**
	 * Return the next id number of the the collection
	 * 
	 * @return the next id
	 * @throws FileNotFoundException
	 */
	public int getNextID() {
		Appointments collection = this.getAppointments();
		int highId = 0;
		for (Appointment a : collection.getAppointments()) {
			if (a.getId() > highId) {
				highId = a.getId();
			}
		}
		highId++;
		return highId;
	}

	/**
	 * This method delete and appoint that is set by its id.
	 * 
	 * @param appointmentID
	 *            to be deleted
	 * @return true if it deletes the elements, false if not
	 * @throws FileNotFoundException
	 */
	public boolean deleteAppointment(int appointmentID) {
		Appointments collection = this.getAppointments();
		Appointment appointment = new Appointment();
		for (Appointment a : collection.getAppointments()) {
			if (a.getId() == appointmentID) {
				appointment = a;
				break;
			}
		}
		collection.getAppointments().remove(appointment);
		this.saveAppointments(collection);

		return true;
	}

	/**
	 * Return and appointment giving the id of it
	 * 
	 * @param appointmentsID
	 * @return The appointment with this appointmentID
	 * @throws FileNotFoundException
	 */
	public Appointment getAppointment(int appointmentID) {
		Appointments collection = this.getAppointments();
		for (Appointment a : collection.getAppointments()) {
			if (a.getId() == appointmentID) {
				return a;
			}
		}
		return null;
	}
	
	/**
	 * This method makes the modification of one appointment and stores it
	 * @param appointment to be modified
	 * @return true if it makes all the changes
	 */
	public boolean modifyAppointment(Appointment appointment){
		Appointment a = this.getAppointment(appointment.getId());
		
		this.deleteAppointment(appointment.getId());
		
		a.setComment(appointment.getComment());
		a.setDate(appointment.getDate());
		a.setDuration(appointment.getDuration());
		a.setHeader(appointment.getHeader());
		
		return this.saveAppointment(a);
	}
}
