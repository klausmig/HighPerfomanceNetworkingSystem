package edu.bonn.hpn.entities;

import java.util.ArrayList;
import java.util.List;

public class Appointments {
	       

	private List<Appointment> appointments;
	
		
	/**
	 * @return the appointments
	 */
	public List<Appointment> getAppointments() {
		return appointments;
	}

	/**
	 * @param appointments the appointments to set
	 */
	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	/**
	 * Class contructor
	 */
	public Appointments(){
		this.appointments = new ArrayList<Appointment>();
	}
}
