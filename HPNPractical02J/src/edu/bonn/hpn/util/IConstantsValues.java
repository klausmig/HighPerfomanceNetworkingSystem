package edu.bonn.hpn.util;

/**
 * Interface that represent the program constants
 * 
 * @author Carlos Montoya
 */
public interface IConstantsValues {
    
    /** */
    public static final String FILE_NAME = "data.xml";
    
    public static final String CONFIG_FILE_NAME = "config.properties";
    
    public static final String PORT = "server.port";
    
    public static final String URL_TO_CONNECT = "server.url.toconnect";
    
    public static final String LOCAL_IP = "server.url.localip";
    
    public static final String ALG_TYPE = "client.algtype";
	
       
}
