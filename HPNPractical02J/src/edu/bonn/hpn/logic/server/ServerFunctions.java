package edu.bonn.hpn.logic.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import edu.bonn.hpn.entities.Appointment;
import edu.bonn.hpn.entities.Appointments;
import edu.bonn.hpn.logic.Manager;
import edu.bonn.hpn.logic.client.TokenManager;
import edu.bonn.hpn.presentation.Program;
import edu.bonn.hpn.util.DateUtil;

/**
 * @author Carlos
 * 
 */
public class ServerFunctions {

	public ServerFunctions() {
	}

	/**
	 * @return
	 */
	public String GetAppointmentsList() {

		Appointments apos = Manager.getAppointments();
		Gson gson = new Gson();
		return gson.toJson(apos.getAppointments());
	}
	
	/**
	 * @return TOKEN RING METHOD ------------------------------------------------------->
	 * 
	 */
	public boolean GiveToken() {
		TokenManager.tokenArrival();
		return true;
	}
	
	/**
	 * @return TOKEN RING METHOD ------------------------------------------------------->
	 * 
	 */
	public boolean IsTokenNode() {
		return TokenManager.haveTheToken;
	}

	/**
	 * 
	 * @return
	 */
	public Appointment[] GetAppointments() {
		Appointments apos = Manager.getAppointments();
		return (Appointment[]) apos.getAppointments().toArray();
	}

	/**
	 * 
	 * @param appointments
	 * @return
	 */
	public boolean SetAppointments(Appointment[] appointments) {
		List<Appointment> appointmentsList = new ArrayList<Appointment>();
		for (Appointment app : appointments) {
			appointmentsList.add(app);
		}
		Appointments aux = new Appointments();
		aux.setAppointments(appointmentsList);
		return Manager.setApointments(aux);
	}

	/**
	 * 
	 * @param appointmentsJson
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public boolean SetAppointmentslist(String appointmentsJson) {
		Gson gson = new Gson();
		Object pdd = gson.fromJson(appointmentsJson, Object.class);
		List response2 = (List) pdd;
		List<Appointment> appointmentsList = new ArrayList<Appointment>();
		for (Object o : response2) {
			Appointment appa = new Appointment();
			appa.setId(((Double) ((LinkedTreeMap) o).get("id")).intValue());
			appa.setDuration(((Double) ((LinkedTreeMap) o).get("duration"))
					.intValue());
			appa.setComment((String) ((LinkedTreeMap) o).get("comment"));
			appa.setHeader((String) ((LinkedTreeMap) o).get("header"));
			String ddd = (String) ((LinkedTreeMap) o).get("date");
			Date test = null;
			try {
				test = new Date(ddd);
			} catch (IllegalArgumentException ex) {
				test = DateUtil.getTimeFromString2(ddd.trim());
			}
			appa.setDate(test);

			appointmentsList.add(appa);
		}

		Appointments aux = new Appointments();
		aux.setAppointments(appointmentsList);
		return Manager.setApointments(aux);
	}

	/**
	 * 
	 * @return
	 */
	public boolean test() {

		return true;
	}

	/**
	 * 
	 * @return
	 */
	public boolean GetConnection() {
		return true;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Appointment GetAppointmentById(int id) {
		return Manager.getAppointmentById(id);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String GetAppointmentByIdString(int id) {
		Appointment app = this.GetAppointmentById(id);
		Gson gson = new Gson();
		return gson.toJson(app);
	}

	/**
	 * 
	 * @param appointment
	 */
	public void DeleteAppointment(Appointment appointment) {
		Manager.deleteAppointment(appointment);
	}

	/**
	 * 
	 * @return
	 */
	public String[] GetHosts() {
		String[] response = new String[Manager.getNetwork().getHosts().size()];
		int i = 0;
		for (String host : Manager.getNetwork().getHosts()) {
			response[i] = host;
			i++;
		}
		return response;
	}

	/**
	 * 
	 * @return
	 */
	public String GetHostsString() {
		Gson gson = new Gson();
		return gson.toJson(Manager.getNetwork().getHosts());
	}

	/**
	 * 
	 * @param host
	 * @return
	 */
	public int RegisterHost(String host) {
		if (!Program.algTypeTR) {
			System.out.println("\n>> Client " + host + " connected. ");
			
			Manager.getNetwork().getAlgRicart().LC++;
			
			
			if (Manager.getNetwork().getAlgRicart().LC == 1)  Manager.getNetwork().getAlgRicart().LC+=1;

			System.out.println("\n>>Client " + host + " connected. ");
	        System.out.println("\n>>LC: "+Manager.getNetwork().getAlgRicart().LC+" , LCClient:1\n ");
			
			if (Manager.getNetwork().getHosts().size() == 0) {
				Manager.getNetwork().getHosts().add(host);
			}

			Manager.getNetwork().setState(true);
			Manager.getNetwork().getAlgRicart().LC ++;  // send reply event the LC value of the server

			for (String hostSaved : Manager.getNetwork().getHosts()) {
				if (host == hostSaved)
					return Manager.getNetwork().getAlgRicart().LC;
			}

			Manager.getNetwork().getHosts().add(host);
			
			System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");
	        
	        return Manager.getNetwork().getAlgRicart().LC;
		} else {
			System.out.println("\n>> Client " + host + " connected. \n>>");
			
			Manager.getNetwork().setState(true);
			Manager.getNetwork().getHosts().add(host);
			
			return Manager.getNetwork().getAlgRicart().LC;
		}
		
	}

	/**
	 * 
	 * @param hosts
	 * @return
	 */
	public boolean SetHosts(String[] hosts) {
		List<String> hostAux = new ArrayList<String>();
		for (String aux : hosts) {
			hostAux.add(aux);
		}
		Manager.getNetwork().setHosts(hostAux);
		return true;
	}

	/**
	 * 
	 * @param hosts
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public boolean SetHostsString(String hosts) {
		Gson gson = new Gson();
		Object pdd = gson.fromJson(hosts, Object.class);
		List response = (List) pdd;
		List<String> hostAux = new ArrayList<String>();
		for (Object hos : response) {
			hostAux.add((String) hos);
		}

		Manager.getNetwork().setHosts(hostAux);
		return true;
	}

	/**
	 * 
	 * @param locked
	 * @return
	 */
	/*
	 * public boolean LockClient(boolean locked) {
	 * 
	 * if (locked) System.out.println(">> This Client is locked.\n>>"); else
	 * System.out.println(">> This Client is unlocked.\n>>");
	 * 
	 * return true; }
	 */

	/**
	 * 
	 * @param messageClient
	 * @return
	 */
	public String requestResource(String messageClient) {

		String[] message = messageClient.split(",");
		int LCClient = Integer.parseInt(message[0]);
		String hostClient = message[1];

		if (Manager.getNetwork().getAlgRicart().isAlreadyRequestResource()) {

			if (Manager.getNetwork().getAlgRicart().LCRequest <= LCClient) {

				System.out.println("\n>> This Host has priority, lesser or equal LC");
				System.out.println(">>LCRequest: {"
						+ Manager.getNetwork().getAlgRicart().LCRequest
						+ "} , LCClient:{" + LCClient + "}, Client:{"
						+ hostClient + "}\n ");
				Manager.getNetwork().getAlgRicart().LC = Math.max(LCClient,Manager.getNetwork().getAlgRicart().LC) + 1;

				if (!(Manager.getNetwork().getAlgRicart().requestQueue.contains(hostClient))) {

					Manager.getNetwork().getAlgRicart().requestQueue.add(hostClient);
					System.out.println(">>host:{" + hostClient + "} added to requestQueue\n ");
				}
				
				Manager.getNetwork().getAlgRicart().LC++;
				System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");

				return Manager.getNetwork().getAlgRicart().LC + ","
						+ Manager.getNetwork().getLocalHost();
			} else {
				System.out.println("\n>>This Host has no priority, greater LC");
				System.out.println(">>LCRequest: {"
						+ Manager.getNetwork().getAlgRicart().LCRequest
						+ "} , LCClient:{" + LCClient + "}, Client:{"
						+ hostClient + "}\n ");
				Manager.getNetwork().getAlgRicart().LC = Math.max(LCClient,
						Manager.getNetwork().getAlgRicart().LC) + 1;

				if (!(Manager.getNetwork().getAlgRicart().replyIPNotOk.contains(hostClient))) {
					Manager.getNetwork().getAlgRicart().replyIPNotOk.add(hostClient);
					System.out.println(">>host:{" + hostClient + "} added to replyIPNotOk\n ");
				}
				Manager.getNetwork().getAlgRicart().LC++;
				System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");
				return Manager.getNetwork().getAlgRicart().LC + "," + "ok";
			}
		}

		if (Manager.getNetwork().getAlgRicart().isUsingResource()) {
			System.out.println("\n>>This Host is using the resource.");
			Manager.getNetwork().getAlgRicart().LC = Math.max(LCClient, Manager
					.getNetwork().getAlgRicart().LC) + 1;
			System.out.println(">>LC: {"
					+ Manager.getNetwork().getAlgRicart().LC + "} , LCClient:{"
					+ LCClient + "}, Client:{" + hostClient + "}\n ");

			if (!(Manager.getNetwork().getAlgRicart().requestQueue
					.contains(hostClient))) {
				Manager.getNetwork().getAlgRicart().requestQueue
						.add(hostClient);
				System.out.println(">>host:{" + hostClient
						+ "} added to requestQueue\n ");
			}
			Manager.getNetwork().getAlgRicart().LC++;
			System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");
			return Manager.getNetwork().getAlgRicart().LC + "," + Manager.getNetwork().getLocalHost();
		}

		System.out
				.println("\n>> This Host no needs the resource editing sending ok.\n");
		Manager.getNetwork().getAlgRicart().LC = Math.max(LCClient, Manager
				.getNetwork().getAlgRicart().LC) + 1;
		Manager.getNetwork().getAlgRicart().LC++;
		System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");
		return Manager.getNetwork().getAlgRicart().LC + "," + "ok";
	}

	/**
	 * Accept a reply release ok from client host
	 * @param messageClient
	 * @return
	 */
	public int replyOK(String messageClient) {
		String[] message = messageClient.split(",");
		int LCClient = Integer.parseInt(message[0]);
		String hostClient = message[1];

		if (Manager.getNetwork().getAlgRicart().isAlreadyRequestResource()) {
			Manager.getNetwork().getAlgRicart().replyIPNotOk.remove(hostClient);
			System.out.println("\n>> Accepting Ok release from " + hostClient+ ".");

			Manager.getNetwork().getAlgRicart().LC = Math.max(LCClient, Manager.getNetwork().getAlgRicart().LC) + 1;
			System.err.println(">>LC: "
					+ Manager.getNetwork().getAlgRicart().LC + " , LCClient:"
					+ LCClient + "\n ");
			System.out.println("LC:" + Manager.getNetwork().getAlgRicart().LC + ">>");
		}

		//Change1
		if (Manager.getNetwork().getAlgRicart().isAlreadyRequestResource() &&  Manager.getNetwork().getAlgRicart().replyIPNotOk.size() == 0) { 
			
			System.out.println("\n>> You have  the edit or delete resource, you can proceed !");
			Manager.getNetwork().getAlgRicart().setUsingResource(true);
			Manager.getNetwork().getAlgRicart().setAlreadyRequestResource(false);
		}

		return Manager.getNetwork().getAlgRicart().LC++;
	}

}
