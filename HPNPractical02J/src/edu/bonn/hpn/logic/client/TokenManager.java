package edu.bonn.hpn.logic.client;

import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import edu.bonn.hpn.logic.Manager;

/**
 * 
 * @author Cristobal
 * 
 */
public class TokenManager {

	public static boolean alreadyRequestToken = false;
	public static boolean haveTheToken = false;
	public static boolean usingToken = false;
	public static String nextHost = null;

	public static boolean onlyNodeInRing = true;

	public static Timer timer = new Timer();

	/**
	 * 
	 */
	public static void passToken() {
		// resetting values
		alreadyRequestToken = false;
		usingToken = false;

		calculateNextHost();
		try {
			// Connecting to specific host...
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setServerURL(new URL(nextHost));
			config.setEnabledForExtensions(true);
			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			Object[] params = new Object[] {};
			boolean connected = (boolean) client.execute(
					"appointments.GetConnection", params);

			if (connected) {
				client.execute("appointments.GiveToken", new Object[] {});
				haveTheToken = false;
			}
		} catch (Exception e) {
			System.out.println(">>Error Connecting to host:" + nextHost);
			Manager.getNetwork().getHosts().remove(nextHost);
			Manager.getNetwork().SynchronizeHost();
			// updateRing(nextHost);
		}

	}

	/**
	 * 
	 */
	public static void tokenArrival() {
		if (alreadyRequestToken && !haveTheToken) {
			haveTheToken = true;
			usingToken = true;
			System.out.println("Number of nodes in Network: "
					+ Manager.getNetwork().getHosts().size());
			System.out
					.println("\n> You have the token, you can access the resource... ");
			System.out.print("Hosts:" + Manager.getNetwork().getHosts().size()
					+ ">>");
		} else {
			haveTheToken = true;
		}
	}

	/**
	 * 
	 */
	public static void startTimer() {
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				if (haveTheToken && !usingToken
						&& Manager.getNetwork().getHosts().size() > 1) {
					passToken();
				}
				if (Manager.getNetwork().getHosts().size() > 1) {
					onlyNodeInRing = false;
				} else if (!onlyNodeInRing) {
					System.out
							.println("\n>>This is the only host in the Network\n");
					onlyNodeInRing = true;
				}
				// System.out.println("Im here");
			}
		}, 0, 200); // run 200 milliseconds
	}

	/**
	 * 
	 */
	public static void calculateNextHost() {
		String localHost = Manager.getNetwork().getLocalHost();
		Object[] strArray = Manager.getNetwork().getHosts().toArray();
		if (strArray.length > 1) {
			for (int i = 0; i < strArray.length; i++) {
				if (strArray[i].toString().equalsIgnoreCase(localHost)
						&& i != (strArray.length - 1)) {
					nextHost = strArray[i + 1].toString();
				} else if (strArray[i].toString().equalsIgnoreCase(localHost)
						&& i == (strArray.length - 1)) {
					nextHost = strArray[0].toString();
				}
			}
		} else {
			nextHost = null;
		}
	}
}
