/**
 * 
 */
package edu.bonn.hpn.logic.client;

import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import edu.bonn.hpn.entities.Appointment;
import edu.bonn.hpn.entities.Appointments;
import edu.bonn.hpn.presentation.Program;
import edu.bonn.hpn.util.DateUtil;
import edu.bonn.hpn.util.IConstantsValues;
import edu.bonn.hpn.util.PropsUtils;

/**
 * @author Carlos
 * 
 */
public class Network {

	private List<String> hosts;
	private String ipLocal;
	private boolean state;
	private Ricart algRicart;

	private Properties configProp = new Properties();
	private Gson gson = new Gson();

	/**
	 * 
	 */
	public Network() {
		setHosts(new ArrayList<String>());
		loadProps();
		getHosts().add(getLocalHost());
		algRicart = new Ricart();
	}

	/**
	 * 
	 * @param url
	 * @return
	 */
	public boolean addHost(String url) {
		this.hosts.add(url);
		return true;
	}

	/**
	 * 
	 * @param host
	 * @return
	 */
	public boolean removeHost(String host) {
		this.hosts.remove(host);
		return true;
	}

	/**
	 * 
	 * @param host
	 * @return
	 */
	public boolean connectHost(String host) {
		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

			config.setServerURL(new URL(host));
			config.setEnabledForExtensions(true);

			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			Object[] params = new Object[] {};
			boolean connected = (boolean) client.execute(
					"appointments.GetConnection", params);

			if (connected) {
				System.out.println(">>The connection to the server: {" + host
						+ "}, was succesful");
				this.hosts.add(host);

				String local = getLocalHost();
				params = new Object[] { local };
				int LCServer = (int) client.execute("appointments.RegisterHost", params);
				
				if(!Program.algTypeTR){
					this.algRicart.LC = Math.max(this.algRicart.LC, LCServer) + 1;
					System.out.println(">>LC: "+this.algRicart.LC+" ,LCServer: "+ LCServer);
				}
				params = new Object[] {};
				Object hostObject = client.execute("appointments.GetHosts",
						params);
				Object[] hostsObject = (Object[]) hostObject;
				List<String> hostsAux = new ArrayList<String>();
				for (Object o : hostsObject) {
					hostsAux.add((String) o);
				}
				this.hosts = hostsAux;
			}
			SynchronizeHost();
			state = true;
			return connected;
			
		} catch (Exception e) {
			System.out.println(">>Error Connecting to host:" + host);
		}
		return false;
	}

	/**
	 * 
	 * @throws UnknownHostException
	 */
	public void SynchronizeHost() {
		String localHost = getLocalHost();
		System.out.println("\n>>Synchronizing other Address Host ...\n ");
		for (String host : this.hosts) {
			try {
				if (!localHost.equals(host)) {
					XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

					config.setServerURL(new URL(host));
					config.setEnabledForExtensions(true);

					XmlRpcClient client = new XmlRpcClient();
					client.setConfig(config);
					Object[] params = new Object[] {};
					boolean connected = (boolean) client.execute(
							"appointments.GetConnection", params);
					if (connected) {
						System.out.println(">>The connection to the server: {"
								+ host + "}, was succesful");

						String param = gson.toJson(this.hosts);
						params = new Object[] { param };
						client.execute("appointments.SetHostsString", params);
					}
				}
			} catch (Exception e) {
				System.out.println(">>Error Connecting to host :" + host);
			}
		}
	}

	/**
	 * 
	 * @param newAppointments
	 */
	public void SynchronizeAppointment(Appointments newAppointments) {
		System.out.println("\n>>Synchronizing Appointments in other Host ...");
		String localHost = getLocalHost();
		List<String> hostsNew = new ArrayList<String>();
		for (String host : this.hosts) {
			try {
				if (!localHost.equals(host)) {
					XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

					config.setServerURL(new URL(host));
					config.setEnabledForExtensions(true);

					XmlRpcClient client = new XmlRpcClient();
					client.setConfig(config);
					Object[] params = new Object[] {};
					boolean connected = (boolean) client.execute(
							"appointments.GetConnection", params);
					if (connected) {
						String stringAppointments = gson.toJson(newAppointments
								.getAppointments());
						params = new Object[] { stringAppointments };
						client.execute("appointments.SetAppointmentslist",
								params);
						hostsNew.add(host);
					}
				}
			} catch (Exception e) {
				// Console.WriteLine("Error Connecting: {0}", e.ToString());
				System.out.println(">>Error Connecting to host:" + host
						+ " ... Deleting ...\n");
				// hosts.Remove(host);
			}
		}
		hostsNew.add(localHost);
		hosts = hostsNew;
		SynchronizeHost();
	}

	/**
	 * 
	 * @param host
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public Appointments getAppointments(String host) {
		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();

			config.setServerURL(new URL(host));
			config.setEnabledForExtensions(true);

			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			Object[] params = new Object[] {};
			String appointments = (String) client.execute(
					"appointments.GetAppointmentsList", params);

			Object appointmentsObject = gson.fromJson(appointments,
					Object.class);
			List response2 = (List) appointmentsObject;

			Appointments ret = new Appointments();
			for (Object o : response2) {
				Appointment app =new Appointment();
				app.setId(((Double) ((LinkedTreeMap) o).get("id")).intValue());
				app.setDuration(((Double) ((LinkedTreeMap) o).get("duration")).intValue());
				app.setComment( (String) ((LinkedTreeMap) o).get("comment"));
				app.setHeader((String) ((LinkedTreeMap) o).get("header"));
				String ddd =  (String) ((LinkedTreeMap) o).get("date");
				Date test = null;
				try{
					test = new Date(ddd);
				}catch(IllegalArgumentException ex){
					test = DateUtil.getTimeFromString2(ddd.trim());
				}
				app.setDate(test);				
				
				ret.getAppointments().add(app);
			}
			return ret;
		} catch (Exception e) {
			System.out.println("Error Connecting to host: " + host);
		}
		return null;
	}

	/**
	 * 
	 * @return
	 */
	public String getLocalIp(){
			//InetAddress IP = InetAddress.getLocalHost();

			Object localIp = configProp.getProperty(IConstantsValues.LOCAL_IP);
			String local = (String) localIp;
			
			return local;
	}
	
	/**
	 * 
	 * @return
	 * @throws UnknownHostException
	 */
	public String getLocalHost() {
			//InetAddress IP = InetAddress.getLocalHost();
			//System.out.println("IP of my system is := " + IP.getHostAddress());
			Object port = configProp.getProperty(IConstantsValues.PORT);
			String portStr = (String) port;
			Object localIp = configProp.getProperty(IConstantsValues.LOCAL_IP);
			String local = (String) localIp;
			
			return "http://" + local + ":" + portStr + "/xmlrpc";
	}

	/**
	 * @return the hosts
	 */
	public List<String> getHosts() {
		return hosts;
	}

	/**
	 * @param hosts
	 *            the hosts to set
	 */
	public void setHosts(List<String> hosts) {
		this.hosts = hosts;
	}

	/**
	 * @return the ipLocal
	 */
	public String getIpLocal() {
		return ipLocal;
	}

	/**
	 * @param ipLocal
	 *            the ipLocal to set
	 */
	public void setIpLocal(String ipLocal) {
		this.ipLocal = ipLocal;
	}

	/**
	 * @return the state
	 */
	public boolean isState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(boolean state) {
		this.state = state;
	}

	/**
	 * 
	 */
	public void loadProps() {
		try {
			setConfigProp(PropsUtils.load(IConstantsValues.CONFIG_FILE_NAME));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the configProp
	 */
	public Properties getConfigProp() {
		return configProp;
	}

	/**
	 * @param configProp
	 *            the configProp to set
	 */
	public void setConfigProp(Properties configProp) {
		this.configProp = configProp;
	}

	/**
	 * @return the gson
	 */
	public Gson getGson() {
		return gson;
	}

	/**
	 * @param gson
	 *            the gson to set
	 */
	public void setGson(Gson gson) {
		this.gson = gson;
	}

	/**
	 * @return the algRicart
	 */
	public Ricart getAlgRicart() {
		return algRicart;
	}

	/**
	 * @param algRicart the algRicart to set
	 */
	public void setAlgRicart(Ricart algRicart) {
		this.algRicart = algRicart;
	}
}
